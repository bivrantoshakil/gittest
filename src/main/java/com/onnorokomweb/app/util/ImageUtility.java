package com.onnorokomweb.app.util;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;


@Component(value = "imageutility")
public class ImageUtility {

    public void saveImage(String imageSavingFormat, int height, int width, String originalImagePathWithNameAndFormat, String savingImagePathWithNameAndFormat, boolean isBlackNWhite){
        try{
            BufferedImage originalImage = ImageIO.read(new File(originalImagePathWithNameAndFormat));
            int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
            BufferedImage resizeImage = resizeImage(originalImage, type, width, height);
            if(isBlackNWhite)
                grayImage(resizeImage,savingImagePathWithNameAndFormat);
            else
                ImageIO.write(resizeImage, imageSavingFormat, new File(savingImagePathWithNameAndFormat));
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    public BufferedImage resizeImage(BufferedImage originalImage, int type, int height, int width){
        BufferedImage resizedImage = new BufferedImage(width, height, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, width, height, null);
        g.dispose();
        return resizedImage;
    }

    public void saveDepartmentImage(MultipartFile photo, int height, int width, String imageSource, String imageDestination, String imageName, boolean isBlackNWhite){
        String imgFormat = "jpg";
        byte[] bytes;
        File file = null;
        BufferedOutputStream stream;
        try {
            if (!photo.isEmpty()) {
                bytes = photo.getBytes();
                file = new File(imageSource, imageName + imgFormat);
                stream = new BufferedOutputStream(new FileOutputStream(file));
                stream.write(bytes);
                stream.close();
                saveImage(imgFormat, height, width, imageSource + imageName + imgFormat, imageDestination + imageName + "." + imgFormat,isBlackNWhite);
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public BufferedImage cropImage(byte[] byteImage, int x, int y, int height, int width) {
        BufferedImage bufferedImage = null;
        if (byteImage != null) {
            try {
                bufferedImage = ImageIO.read(new ByteArrayInputStream(byteImage));
                if(height<1 || width<1) {
                   return bufferedImage;
                }
                else {
                   return bufferedImage.getSubimage(x, y, height, width);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        else
            return null;
    }

    public void grayImage(BufferedImage originalImage, String destinationPath) throws IOException {
        BufferedImage blackAndWhiteImg = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        Graphics2D graphics = blackAndWhiteImg.createGraphics();
        graphics.drawImage(originalImage, 0, 0, null);
        ImageIO.write(blackAndWhiteImg, "jpg", new File(destinationPath));
    }

}
