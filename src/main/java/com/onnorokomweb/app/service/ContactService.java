package com.onnorokomweb.app.service;

import com.onnorokomweb.app.entity.ContactUs;
import com.onnorokomweb.app.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;


@Component(value = "contactservice")
public class ContactService {

    @Autowired
    ContactRepository contactRepository;

    public void saveContactUs(ContactUs contactUs) {
        contactUs.setStatus("0");
        contactRepository.save(contactUs);
    }

    public List<ContactUs> getAllContact() {
        return (List<ContactUs>)contactRepository.findAll();
    }

}
