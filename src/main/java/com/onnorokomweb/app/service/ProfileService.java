package com.onnorokomweb.app.service;

import com.onnorokomweb.app.entity.Profile;
import com.onnorokomweb.app.entity.Role;
import com.onnorokomweb.app.entity.User;
import com.onnorokomweb.app.repository.ProfileRepository;
import com.onnorokomweb.app.util.ImageUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;


@Component(value = "profileservice")
public class ProfileService {

    @Autowired
    ProfileRepository profileRepository;
    @Autowired
    RoleService roleService;
    @Autowired
    UserService userService;
    @Autowired
    ImageUtility imageUtility;

    public boolean saveProfileAndUser(Profile profile, MultipartFile photo, String password,int x, int y,int height,int width) {
        boolean isSaved=false;
        BufferedImage bufferedImage=null;
        User user=null;
        List<Role> roles=new ArrayList<>();
        roles.add(roleService.getById(2));
        try {
            if (!photo.isEmpty()) {
                byte[] bytes = photo.getBytes();
                bufferedImage=imageUtility.cropImage(bytes,x,y,height,width);
                File outputImage = new File("src/main/resources/static/img/profile/"+profile.getEmail()+".jpg");
                ImageIO.write(imageUtility.resizeImage(bufferedImage,bufferedImage.getType(),300,300),"jpg", outputImage);
                profile.setImage(profile.getEmail() + ".jpg");
            }
            if (profileRepository.exists(profile.getId())) {
                if (profile.getImage() == null || profile.getImage().trim().equals("")) {
                    profile.setImage(getById(profile.getId()).getImage());
                }
                profileRepository.save(profile);
            } else {
                profileRepository.save(profile);
            }
            user = new User();
            if(profile.getId()!=0) {
                user.setId(profile.getId());
            }
            user.setRoles(roles);
            user.setStatus(User.Status.PENDING);
            user.setEmail(profile.getEmail());
            if(password!=null && !"".equals(password)) {
                user.setPassword(password);
            }else{
                user.setPassword(profile.getNickName());
            }
            user.setProfile(profile);
            //userService.editRole(user);
            isSaved=true;
        }catch (IOException e){
            e.printStackTrace();
            isSaved=false;
        }
        return isSaved;
    }

    public Profile getById(int id) {
        return profileRepository.findById(id);
    }

    public Profile getByEmail(String email) {
        return profileRepository.findByEmail(email);
    }

    public List<Profile> getAllProfile() {
        return (List<Profile>) profileRepository.findAll();
    }

    public List<Profile> getAllProfileOrderByNickNameAsc() {
        return profileRepository.findAllOrderByNickName();
     }

    public List<Profile> getAllProfileWhereStatus() {
        return profileRepository.findAllWhereStatus();
    }

    public boolean deleteProfile(int id) {
        boolean boo = false;
        try {
            profileRepository.delete(id);
            boo = true;
        } catch (Exception e) {
            e.printStackTrace();
            boo = false;
        }
        return boo;
    }

}
