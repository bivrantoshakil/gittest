package com.onnorokomweb.app.service;

import com.onnorokomweb.app.entity.Role;
import com.onnorokomweb.app.entity.User;
import com.onnorokomweb.app.repository.RoleRepository;
import com.onnorokomweb.app.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component(value = "userservice")
public class UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;

    public boolean editRole(User user, String role) {
        boolean isSaved=false;
        List<Role> userRoles = new ArrayList<Role>();
        try {
            if(role.length() !=0) {
                String[] strRoles = role.split(",");
                for (int i = 0; i < strRoles.length; i++) {
                    Role userRols = roleRepository.findOne(Integer.parseInt(strRoles[i].trim()));
                    if (userRols != null) {
                        userRoles.add(userRols);
                    }
                }
                user.setRoles(userRoles);
                userRepository.save(user);
            }
            else {
                user.setRoles(getById(user.getId()).getRoles());
                userRepository.save(user);
            }
            if (userRepository.exists(user.getId())) {
                if (user.getPassword() == null) {
                    user.setPassword(getById(user.getId()).getPassword());
                    userRepository.save(user);
                } else {
                    userRepository.save(user);
                }
                isSaved = true;
            }
            }catch (Exception e){
                e.printStackTrace();
                isSaved=false;
            }
        return isSaved;
    }

    public User getById(int id) {
        return userRepository.findById(id);
    }

    public User getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public List<User> getAllUser() {
        return (List<User>)userRepository.findAll();
    }

}
