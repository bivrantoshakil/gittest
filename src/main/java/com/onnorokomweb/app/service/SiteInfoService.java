package com.onnorokomweb.app.service;

import com.onnorokomweb.app.entity.SiteInfo;
import com.onnorokomweb.app.repository.SiteInfoRepository;
import com.onnorokomweb.app.util.ImageUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;


@Component(value="siteinfoservice")
public class SiteInfoService {

    @Autowired
    SiteInfoRepository siteInfoRepository;
    @Autowired
    ImageUtility imageUtility;

    public List<SiteInfo> getAllSiteInfo() {
        return (List<SiteInfo>)siteInfoRepository.findAll();
    }

    public SiteInfo getById(int id) {
        return siteInfoRepository.findById(id);
    }

    public boolean editSiteInfo(SiteInfo siteInfo ,MultipartFile photo,String temporaryPath) {
        boolean isSaved=false;
        String nameOfImage = siteInfo.getTitle().toLowerCase();
        if(siteInfoRepository.exists(siteInfo.getId())){
            try {
                if (!photo.isEmpty()) {
                    siteInfo.setContent(nameOfImage + ".jpg");
                    imageUtility.saveDepartmentImage(photo, 1140, 450, temporaryPath, temporaryPath, nameOfImage, false);
                }
                siteInfoRepository.save(siteInfo);
                isSaved=true;

            } catch (Exception e) {
                e.printStackTrace();
                isSaved=false;
            }
        }
        return isSaved;
    }

}
