package com.onnorokomweb.app.service;

import com.onnorokomweb.app.entity.Role;
import com.onnorokomweb.app.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;


@Component(value = "roleservice")
public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    public List<Role> getAllRole() {
        return (List<Role>)roleRepository.findAll();
    }

    public Role getById(int id) {
        return roleRepository.findById(id);
    }

}
