package com.onnorokomweb.app.service;

import com.onnorokomweb.app.entity.Role;
import com.onnorokomweb.app.entity.User;
import com.onnorokomweb.app.entity.common.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class CurrentUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;
    private String[] roleNameList;

    public CurrentUser loadUserByUsername(String email){
        User user =null;
        try {
            user = userService.getByEmail(email);
            List<Role> roleList=user.getRoles();
            roleNameList=new String[roleList.size()];
            for(int i=0;i<roleList.size();i++) {
                roleNameList[i]=roleList.get(i).getName();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        if(user == null) {
            throw new UsernameNotFoundException(String.format("User with email=%s was not found", email));
        }
        return new CurrentUser(user,roleNameList);
    }

}
