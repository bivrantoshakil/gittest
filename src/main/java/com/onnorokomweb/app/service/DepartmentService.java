package com.onnorokomweb.app.service;

import com.onnorokomweb.app.entity.Department;
import com.onnorokomweb.app.entity.Profile;
import com.onnorokomweb.app.repository.DepartmentRepository;
import com.onnorokomweb.app.repository.ProfileRepository;
import com.onnorokomweb.app.util.ImageUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;


@Component(value = "departmentservice")
public class DepartmentService {

    @Autowired
    DepartmentRepository departmentRepository;
    @Autowired
    ImageUtility imageUtility;
    @Autowired
    ProfileRepository profileRepository;

    public Department getById(int id){
        return departmentRepository.findById(id);
    }

    public List<Department> getAllDepartment(){
        return (List<Department>)departmentRepository.findAll();
    }

    public boolean saveDepartment(Department department, MultipartFile photo1, MultipartFile photo2, MultipartFile photo3, MultipartFile photo4, MultipartFile photo5, String temporaryPath, String maxPath, String minPath, boolean isDeletePhoto1, boolean isDeletePhoto2, boolean isDeletePhoto3, boolean isDeletePhoto4, boolean isDeletePhoto5) {
        boolean isSavedDepartment;
        String nameOfImage = department.getName().trim().replaceAll("\\s","").toLowerCase();

        try {

            if (!photo1.isEmpty()) {
                department.setImage1(nameOfImage + "1_max.jpg");
                department.setImage1_min(nameOfImage + "1_min.jpg");
                imageUtility.saveDepartmentImage(photo1, 1140, 450, temporaryPath, maxPath, nameOfImage + "1_max", false);
                imageUtility.saveDepartmentImage(photo1, 560, 220, temporaryPath, minPath, nameOfImage + "1_min", true);
            }
            if (!photo2.isEmpty()) {
                department.setImage2(nameOfImage + "2_max.jpg");
                department.setImage2_min(nameOfImage + "2_min.jpg");
                imageUtility.saveDepartmentImage(photo2, 1140, 450, temporaryPath, maxPath, nameOfImage + "2_max", false);
                imageUtility.saveDepartmentImage(photo2, 560, 220, temporaryPath, minPath, nameOfImage + "2_min", true);
            }
            if (!photo3.isEmpty()) {
                department.setImage3(nameOfImage + "3_max.jpg");
                department.setImage3_min(nameOfImage + "3_min.jpg");
                imageUtility.saveDepartmentImage(photo3, 1140, 450, temporaryPath, maxPath, nameOfImage + "3_max", false);
                imageUtility.saveDepartmentImage(photo3, 560, 220, temporaryPath, minPath, nameOfImage + "3_min", true);
            }
            if (!photo4.isEmpty()) {
                department.setImage4(nameOfImage + "4_max.jpg");
                department.setImage4_min(nameOfImage + "4_min.jpg");
                imageUtility.saveDepartmentImage(photo4, 1140, 450, temporaryPath, maxPath, nameOfImage + "4_max", false);
                imageUtility.saveDepartmentImage(photo4, 560, 220, temporaryPath, minPath, nameOfImage + "4_min", true);
            }
            if (!photo5.isEmpty()) {
                department.setImage5(nameOfImage + "5_max.jpg");
                department.setImage5_min(nameOfImage + "5_min.jpg");
                imageUtility.saveDepartmentImage(photo5, 1140, 450, temporaryPath, maxPath, nameOfImage + "5_max", false);
                imageUtility.saveDepartmentImage(photo5, 560, 220, temporaryPath, minPath, nameOfImage + "5_min", true);
            }

            if (departmentRepository.exists(department.getId())) {
                if ((department.getImage1() == null || department.getImage1().trim().equals("")) && !isDeletePhoto1){
                    department.setImage1(getById(department.getId()).getImage1());
                    department.setImage1_min(getById(department.getId()).getImage1_min());
                }
                if ((department.getImage2() == null || department.getImage2().trim().equals("")) && !isDeletePhoto2){
                    department.setImage2(getById(department.getId()).getImage2());
                    department.setImage2_min(getById(department.getId()).getImage2_min());
                }
                if ((department.getImage3() == null || department.getImage3().trim().equals("")) && !isDeletePhoto3){
                    department.setImage3(getById(department.getId()).getImage3());
                    department.setImage3_min(getById(department.getId()).getImage3_min());
                }
                if ((department.getImage4() == null || department.getImage4().trim().equals("")) && !isDeletePhoto4){
                    department.setImage4(getById(department.getId()).getImage4());
                    department.setImage4_min(getById(department.getId()).getImage4_min());
                }
                if ((department.getImage5() == null || department.getImage5().trim().equals("")) && !isDeletePhoto5){
                    department.setImage5(getById(department.getId()).getImage5());
                    department.setImage5_min(getById(department.getId()).getImage5_min());
                }
            }
            departmentRepository.save(department);
            isSavedDepartment = true;
        } catch (Exception e) {
            e.printStackTrace();
            isSavedDepartment = false;
        }

        return isSavedDepartment;
    }

    public boolean deleteDepartment(int id) {
        boolean isDeleted = false;
        try {
            departmentRepository.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            e.printStackTrace();
            isDeleted = false;
        }
        return isDeleted;
    }

    public List<Department> getAllOrderByDepartment(){
        return departmentRepository.findAllOrderByName();
    }

    public List<Profile> getAllProfileByDepartment(int id){
          return profileRepository.findAllStatusAndDept(id);
    }

}
