package com.onnorokomweb.app.controller.client;

import com.onnorokomweb.app.entity.Department;
import com.onnorokomweb.app.entity.Profile;
import com.onnorokomweb.app.entity.SiteInfo;
import com.onnorokomweb.app.service.DepartmentService;
import com.onnorokomweb.app.service.ProfileService;
import com.onnorokomweb.app.service.SiteInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@Controller
@RequestMapping("/client/department")
public class ClientDepartmentController {

    @Autowired
    DepartmentService departmentService;
    @Autowired
    SiteInfoService siteInfoService;
    @Autowired
    ProfileService profileService;

    private final String DEPARTMENT_LIST = "client/department_list";
    private final String DEPARTMENT_DETAILS = "client/department_details";

    @RequestMapping(value="/list")
    public String ourTeam(Model model) {
        List<SiteInfo> siteInfoList =  siteInfoService.getAllSiteInfo();
        List<Department> departmentList =  departmentService.getAllOrderByDepartment();
        model.addAttribute("departmentList", departmentList);
        model.addAttribute("siteInfoList", siteInfoList);
        return DEPARTMENT_LIST;
    }

    @RequestMapping(value="/images/dept/{id}", method={RequestMethod.POST}, produces="application/json")
    @ResponseBody
    public List<String> ourTeam2(@PathVariable(value = "id") int id) {
        Department department = departmentService.getById(id);
        return department.getMinImages();
    }

    @RequestMapping(value="/{id}")
    public String departmentDetails(@PathVariable(value = "id") int id,Model model) {
        List<Profile> deptprofilelist = departmentService.getAllProfileByDepartment(id);
        model.addAttribute("deptprofilelist", deptprofilelist);
        model.addAttribute("departmentdetails", departmentService.getById(id));
        List<SiteInfo> siteInfoList =  siteInfoService.getAllSiteInfo();
        model.addAttribute("siteInfoList", siteInfoList);
        Department departmentImageList =  departmentService.getById(id);
        model.addAttribute("departmentImageList", departmentImageList.getMaxImages());
        return DEPARTMENT_DETAILS;
    }

}
