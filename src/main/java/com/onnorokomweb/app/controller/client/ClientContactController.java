package com.onnorokomweb.app.controller.client;

import com.onnorokomweb.app.entity.ContactUs;
import com.onnorokomweb.app.entity.SiteInfo;
import com.onnorokomweb.app.service.ContactService;
import com.onnorokomweb.app.service.SiteInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.validation.Valid;
import java.util.List;


@Controller
@RequestMapping("/client/contact")
public class ClientContactController {

    @Autowired
    ContactService contactService;
    @Autowired
    SiteInfoService siteInfoService;

    private static final String VIEW_PATH = "/";
    private static final String CONTACT_PART = VIEW_PATH+"#contact";

    @RequestMapping(value = "/add", method = { RequestMethod.POST , RequestMethod.GET})
    public String contactUsSave(@ModelAttribute("contacts")@Valid ContactUs contactUs, BindingResult bindingResult ,Model model) {
        if(bindingResult.hasErrors()) {
            return CONTACT_PART;
        }
        try {
            contactService.saveContactUs(contactUs);
            List<SiteInfo> siteInfoList =  siteInfoService.getAllSiteInfo();
            model.addAttribute("siteInfoList", siteInfoList);
            model.addAttribute("message", "Successfull Send");
        } catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:"+CONTACT_PART;
    }

}
