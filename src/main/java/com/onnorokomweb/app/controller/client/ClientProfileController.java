package com.onnorokomweb.app.controller.client;

import com.onnorokomweb.app.entity.*;
import com.onnorokomweb.app.service.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@Controller
@RequestMapping("/client/profile")
public class ClientProfileController {

    @Autowired
    ProfileService profileService;
    @Autowired
    SiteInfoService siteInfoService;
    @Autowired
    DepartmentService departmentService;

    private final String PROFILE_LIST ="client/profile_list";
    private final String PROFILE_DETAILS ="client/profile_details";

    @RequestMapping(value="/{id}")
    public String clientProfileDetails(@PathVariable(value = "id") int id,Model model) {
        model.addAttribute("profile", profileService.getById(id));
        List<SiteInfo> siteInfoList =  siteInfoService.getAllSiteInfo();
        model.addAttribute("siteInfoList", siteInfoList);
        return PROFILE_DETAILS;
    }

    @RequestMapping(value="/list")
    public String clientProfileList(Model model) {
        List<SiteInfo> siteInfoList =  siteInfoService.getAllSiteInfo();
        model.addAttribute("siteInfoList", siteInfoList);
        model.addAttribute("profilesList", profileService.getAllProfileWhereStatus());
        return PROFILE_LIST;
    }

}
