package com.onnorokomweb.app.controller;

import com.onnorokomweb.app.entity.ContactUs;
import com.onnorokomweb.app.entity.SiteInfo;
import com.onnorokomweb.app.entity.User;
import com.onnorokomweb.app.service.SiteInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    SiteInfoService siteInfoService;

    private final String VIEW_PATH = "/";
    private final String HOME_PAGE = VIEW_PATH + "home";
    private final String ADMIN_CONSOLE = VIEW_PATH + "admin/adminconsole";

    @RequestMapping(value = "")
    public String home(Model model, ContactUs contacts) {
        List<SiteInfo> siteInfoList =  siteInfoService.getAllSiteInfo();
        model.addAttribute("siteInfoList", siteInfoList);
        model.addAttribute("contacts", contacts);
        return HOME_PAGE;
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String login(@RequestParam(value = "error", required = false) String error, @ModelAttribute("user") User user, Model model) {
        if(error != null) {
            model.addAttribute("message", "The email or password you have entered is invalid, try again.");
        }
        List<SiteInfo> siteInfoList =  siteInfoService.getAllSiteInfo();
        model.addAttribute("siteInfoList", siteInfoList);
        return this.VIEW_PATH + "login";
    }

    @RequestMapping(value = "admin")
    public String adminHome() {
        return ADMIN_CONSOLE;
    }

}
