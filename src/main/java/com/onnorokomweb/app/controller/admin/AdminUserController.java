package com.onnorokomweb.app.controller.admin;

import com.onnorokomweb.app.entity.Profile;
import com.onnorokomweb.app.entity.Role;
import com.onnorokomweb.app.entity.User;
import com.onnorokomweb.app.service.ProfileService;
import com.onnorokomweb.app.service.RoleService;
import com.onnorokomweb.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import javax.validation.Valid;
import java.util.List;


@Controller
@RequestMapping("/admin/role")
public class AdminUserController {

    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;
    @Autowired
    ProfileService profileService;

    private final String VIEW_PATH = "/admin/";
    private final String ROLE_ADD = VIEW_PATH + "common/userrole_add";
    private final String PROFILE_LISTS = VIEW_PATH+"profile/list";

    @RequestMapping(value="/{id}")
    public String useradmin(@PathVariable(value = "id") int id,Model model ,User user ) {
        List<Role> roleList =roleService.getAllRole();
        List<Role> roleId=null;
        user=null;
        try {
            user = userService.getById(id);
            roleId=user.getRoles();
        }catch (Exception e){
            e.printStackTrace();
        }
        model.addAttribute("user", user);
        model.addAttribute("roleId", roleId);
        model.addAttribute("roleList", roleList);
        model.addAttribute("userRoleList", user.getRoles());
        return ROLE_ADD;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String userRoleEdit(@ModelAttribute("user") @Valid User user,Model model, BindingResult bindingResult ,WebRequest webRequest) {
        if (bindingResult.hasErrors()) {
             return ROLE_ADD +user.getId();
        }
        String userRole=null;
        userRole=webRequest.getParameter("hdnroles");
        boolean isSaved= userService.editRole(user, userRole);
        if(isSaved) {
            List<Profile> profilesList =  profileService.getAllProfileOrderByNickNameAsc();
            model.addAttribute("profilesList", profilesList);
            return "redirect:"+PROFILE_LISTS;
        }
        else{
            return ROLE_ADD+"/"+ user.getId();
        }
    }

}

