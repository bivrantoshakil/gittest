package com.onnorokomweb.app.controller.admin;

import com.onnorokomweb.app.entity.Department;
import com.onnorokomweb.app.service.DepartmentService;
import com.onnorokomweb.app.service.ProfileService;
import com.onnorokomweb.app.service.SiteInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.util.List;


@Controller
@RequestMapping(value = "/admin/department")
public class AdminDepartmentController {

    @Autowired
    DepartmentService departmentService;
    @Autowired
    SiteInfoService siteInfoService;
    @Autowired
    ProfileService profileService;

    private final String DEPARTMENT_VIEW = "/admin/department/";
    private final String DEPARTMENT_EDIT = DEPARTMENT_VIEW +"department_edit";
    private final String DEPARTMENT_LIST = DEPARTMENT_VIEW + "department_list";
    private final String DEPARTMENT_LISTS=DEPARTMENT_VIEW+"list";
    private final String temporaryPath = "src/main/resources/static/img/department/";
    private final String maxPath = temporaryPath + "max/";
    private final String minPath = temporaryPath + "min/";

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String departmentSave(Model model,@ModelAttribute("department") @Valid Department department, BindingResult bindingResult, @RequestParam("photo1") MultipartFile photo1, @RequestParam("photo2") MultipartFile photo2, @RequestParam("photo3") MultipartFile photo3, @RequestParam("photo4") MultipartFile photo4, @RequestParam("photo5") MultipartFile photo5, @RequestParam("delete_photo1") String deletePhoto1, @RequestParam("delete_photo2") String deletePhoto2, @RequestParam("delete_photo3") String deletePhoto3, @RequestParam("delete_photo4") String deletePhoto4, @RequestParam("delete_photo5") String deletePhoto5) throws Exception{
        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getAllErrors());
            return DEPARTMENT_EDIT;
        }

        boolean isDeletePhoto1, isDeletePhoto2, isDeletePhoto3, isDeletePhoto4, isDeletePhoto5;
        isDeletePhoto1 = deletePhoto1.equals("photo1") ? true : false;
        isDeletePhoto2 = deletePhoto2.equals("photo2") ? true : false;
        isDeletePhoto3 = deletePhoto3.equals("photo3") ? true : false;
        isDeletePhoto4 = deletePhoto4.equals("photo4") ? true : false;
        isDeletePhoto5 = deletePhoto5.equals("photo5") ? true : false;

        boolean isSaved = departmentService.saveDepartment(department, photo1, photo2, photo3, photo4, photo5, temporaryPath, maxPath, minPath, isDeletePhoto1, isDeletePhoto2, isDeletePhoto3, isDeletePhoto4, isDeletePhoto5);
        if (isSaved) {
            List<Department> departmentList =  departmentService.getAllDepartment();
            model.addAttribute("departmentList", departmentList);
            return "redirect:"+DEPARTMENT_LISTS;
        } else {
            return DEPARTMENT_EDIT;
        }
    }

    @RequestMapping(value = "/add")
    public String addDepartment(Department department) {
        return DEPARTMENT_EDIT;
    }

    @RequestMapping(value = "/list")
    public String allDepartment(Model model) {
        List<Department> departmentList =  departmentService.getAllDepartment();
        model.addAttribute("departmentList", departmentList);
        return DEPARTMENT_LIST;
    }

    @RequestMapping(value="/edit/{id}")
    public String editDepartment(@PathVariable(value = "id") int id, Model model) {
        model.addAttribute("department", departmentService.getById(id));
        return DEPARTMENT_EDIT;
    }

    @RequestMapping(value = "/delete/{id}")
    public String deleteDepartment(Model model,@PathVariable(value = "id") int id) {
        boolean isDeleted = departmentService.deleteDepartment(id);
        if (isDeleted) {
            List<Department> departmentList =  departmentService.getAllDepartment();
            model.addAttribute("departmentList", departmentList);
            return "redirect:"+DEPARTMENT_LISTS;
        } else {
            List<Department> departmentList =  departmentService.getAllDepartment();
            model.addAttribute("departmentList", departmentList);
            return "redirect:"+DEPARTMENT_LISTS;
        }
    }

}
