package com.onnorokomweb.app.controller.admin;

import com.onnorokomweb.app.entity.SiteInfo;
import com.onnorokomweb.app.service.SiteInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.util.List;


@Controller
@RequestMapping("/admin/siteinfo")
public class AdminSiteInfoController {

    @Autowired
    SiteInfoService siteInfoService;

    private final String VIEW_PATH = "/";
    private final String SITEINFO_EDIT=VIEW_PATH+"admin/common/siteinfo_edit";
    private final String SITEINFO_LIST=VIEW_PATH+"admin/common/siteinfo_list";
    private final String temporaryPath = "src/main/resources/static/img/siteinfo/";
    private final String SITEINFO_LISTS=VIEW_PATH+"admin/siteinfo/list";

    @RequestMapping(value="/list")
    public String getAllSiteinfo(Model model) {
        List<SiteInfo> siteInfoList =  siteInfoService.getAllSiteInfo();
        model.addAttribute("siteInfoList", siteInfoList);
        return SITEINFO_LIST;
    }

    @RequestMapping(value="/edit/{id}")
    public String getSiteInfoById(@PathVariable(value = "id") int id,Model model) {
        model.addAttribute("editsiteinfo", siteInfoService.getById(id));
        return SITEINFO_EDIT;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String siteInfoEdit(@ModelAttribute("siteInfo")@Valid SiteInfo siteInfo, BindingResult bindingResult ,Model model, @RequestParam("contentImg") MultipartFile photo) throws Exception{
        if(bindingResult.hasErrors()) {
            return "redirect:"+SITEINFO_LIST;
        }
        boolean isSaved= siteInfoService.editSiteInfo(siteInfo, photo, temporaryPath);
        if(isSaved) {
            List<SiteInfo> siteInfoList =  siteInfoService.getAllSiteInfo();
            model.addAttribute("siteInfoList", siteInfoList);
            return "redirect:"+SITEINFO_LISTS;
        }
        else{
            return  SITEINFO_EDIT;
        }
    }

}
