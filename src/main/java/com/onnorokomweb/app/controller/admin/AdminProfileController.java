package com.onnorokomweb.app.controller.admin;

import com.onnorokomweb.app.entity.*;
import com.onnorokomweb.app.service.*;
import com.onnorokomweb.app.validation.ProfileSaveValidator;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.util.List;


@Controller
@RequestMapping("/admin/profile")
public class AdminProfileController {

    @Autowired
    ProfileService profileService;
    @Autowired
    SiteInfoService siteInfoService;
    @Autowired
    DepartmentService departmentService;
    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;
    @Autowired
    ProfileSaveValidator profileSaveValidator;

    private final String PROFILE_VIEW = "/admin/profile/";
    private final String PROFILE_EDIT = PROFILE_VIEW+"profile_edit";
    private final String PROFILE_ALL = PROFILE_VIEW+"profile_list";
    private final String PROFILE_LISTS = PROFILE_VIEW+"list";

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String profileSave(@ModelAttribute("profile") @Valid Profile profile, BindingResult bindingResult, @RequestParam("photo") MultipartFile photo, @RequestParam("password") String password, @RequestParam("x1") int x1, @RequestParam("x2") int x2, @RequestParam("y1") int y1, @RequestParam("y2") int y2, Model model) throws Exception{
        profileSaveValidator.validate(profile, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("alldepartment", departmentService.getAllDepartment());
            return PROFILE_EDIT;
        }
        boolean isSaved= profileService.saveProfileAndUser(profile,photo,password,x1,y1,(x2-x1),(y2-y1));
        if(isSaved) {
            List<Profile> profilesList =  profileService.getAllProfileOrderByNickNameAsc();
            model.addAttribute("profilesList", profilesList);
            return "redirect:"+PROFILE_LISTS;
        }
        else{
            return  PROFILE_EDIT;
        }
    }

    @RequestMapping(value="/add")
    public String addProfile(Profile profile, Model model) {
        model.addAttribute("alldepartment", departmentService.getAllDepartment());
        return PROFILE_EDIT;
    }


    @RequestMapping(value="/list")
    public String allProfile(Model model) {
        List<Profile> profilesList =  profileService.getAllProfileOrderByNickNameAsc();
        model.addAttribute("profilesList", profilesList);
        return PROFILE_ALL;
    }

    @RequestMapping(value="/edit/{id}")
    public String editProfile(@PathVariable(value = "id") int id,Model model) {
        model.addAttribute("profile", profileService.getById(id));
        model.addAttribute("alldepartment", departmentService.getAllDepartment());
        return PROFILE_EDIT;
    }

    @RequestMapping(value="/delete/{id}")
    public String deleteProfile(@PathVariable(value = "id") int id, Model model) {
        boolean isDeleted = profileService.deleteProfile(id);
        List<Profile> profilesList =  profileService.getAllProfileOrderByNickNameAsc();
        model.addAttribute("profilesList", profilesList);
        return "redirect:"+PROFILE_LISTS;
    }

}
