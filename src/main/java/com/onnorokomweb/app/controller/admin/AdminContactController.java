package com.onnorokomweb.app.controller.admin;

import com.onnorokomweb.app.entity.ContactUs;
import com.onnorokomweb.app.service.ContactService;
import com.onnorokomweb.app.service.SiteInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;


@Controller
@RequestMapping("/admin/contact")
public class AdminContactController {

    @Autowired
    ContactService contactService;
    @Autowired
    SiteInfoService siteInfoService;

    private final String CONTACT_LIST="admin/common/contact_list";

    @RequestMapping(value="/list")
    public String contactList(Model model) {
        List<ContactUs> contactUsList =  contactService.getAllContact();
        model.addAttribute("contactUsList", contactUsList);
        return CONTACT_LIST;
    }

}
