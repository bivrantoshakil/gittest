package com.onnorokomweb.app.entity;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "department")
public class Department{

    public enum Status {ACTIVE, INACTIVE}
    private int id;
    private String name;
    private String nameBn;
    private String description;
    private String descriptionBn;
    private Status status;
    private String image1;
    private String image2;
    private String image3;
    private String image4;
    private String image5;
    private String image1_min;
    private String image2_min;
    private String image3_min;
    private String image4_min;
    private String image5_min;
    private Timestamp created;
    private Timestamp updated;
    private List<Profile> profileList;

    public Department() {
        this.status = Status.ACTIVE;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotBlank
    @Column(name = "name")
    @Length(max = 56)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "name_bn")
    @Length(max = 56)
    public String getNameBn() {
        return nameBn;
    }

    public void setNameBn(String nameBn) {
        this.nameBn = nameBn;
    }

    @NotBlank
    @Column(name = "description")
    @Length(max = 1024)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "description_bn")
    @Length(max = 1024)
    public String getDescriptionBn() {
        return descriptionBn;
    }

    public void setDescriptionBn(String descriptionBn) {
        this.descriptionBn = descriptionBn;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Column(name = "image1")
    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    @Column(name = "image2")
    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    @Column(name = "image3")
    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    @Column(name = "image4")
    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    @Column(name = "image5")
    public String getImage5() {
        return image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    @Transient
    public List<String> getMaxImages(){
        List<String> maxImages = null;
        try {
            maxImages = new ArrayList<>();
            if (null != image1 && !image1.trim().equals(""))
                maxImages.add(image1);
            if (null != image2 && !image2.trim().equals(""))
                maxImages.add(image2);
            if (null != image3 && !image3.trim().equals(""))
                maxImages.add(image3);
            if (null != image4 && !image4.trim().equals(""))
                maxImages.add(image4);
            if (null != image5 && !image5.trim().equals(""))
                maxImages.add(image5);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return maxImages;
    }

    @Column(name = "image1_min")
    public String getImage1_min() {
        return image1_min;
    }

    public void setImage1_min(String image1_min) {
        this.image1_min = image1_min;
    }

    @Column(name = "image2_min")
    public String getImage2_min() {
        return image2_min;
    }

    public void setImage2_min(String image2_min) {
        this.image2_min = image2_min;
    }

    @Column(name = "image3_min")
    public String getImage3_min() {
        return image3_min;
    }

    public void setImage3_min(String image3_min) {
        this.image3_min = image3_min;
    }

    @Column(name = "image4_min")
    public String getImage4_min() {
        return image4_min;
    }

    public void setImage4_min(String image4_min) {
        this.image4_min = image4_min;
    }

    @Column(name = "image5_min")
    public String getImage5_min() {
        return image5_min;
    }

    public void setImage5_min(String image5_min) {
        this.image5_min = image5_min;
    }

    @Transient
    public List<String> getMinImages(){
        List<String> minImages = null;
        try {
            minImages = new ArrayList<>();
            if (null != image1_min && !image1_min.trim().equals(""))
                minImages.add(image1_min);
            if (null != image2_min && !image2_min.trim().equals(""))
                minImages.add(image2_min);
            if (null != image3_min && !image3_min.trim().equals(""))
                minImages.add(image3_min);
            if (null != image4_min && !image4_min.trim().equals(""))
                minImages.add(image4_min);
            if (null != image5_min && !image5_min.trim().equals(""))
                minImages.add(image5_min);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return minImages;
    }

    @OneToMany(mappedBy = "department")
    public List<Profile> getProfileList() {
        return profileList;
    }

    public void setProfileList(List<Profile> profileList) {
        this.profileList = profileList;
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Column(name = "updated")
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @PrePersist
    public void onCreate() {
        created = new Timestamp(new Date().getTime());
    }

    @PreUpdate
    public void onUpdate() {
        updated = new Timestamp(new Date().getTime());
    }

    @Override
    public String toString() {
        return "Department{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", status='" + status + '\'' +
            ", image1='" + image1 + '\'' +
            ", image2='" + image2 + '\'' +
            ", image3='" + image3 + '\'' +
            ", image4='" + image4 + '\'' +
            ", image5='" + image5 + '\'' +
            ", image1_min='" + image1_min + '\'' +
            ", image2_min='" + image2_min + '\'' +
            ", image3_min='" + image3_min + '\'' +
            ", image4_min='" + image4_min + '\'' +
            ", image5_min='" + image5_min + '\'' +
            '}';
    }

}
