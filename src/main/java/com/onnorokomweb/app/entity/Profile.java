package com.onnorokomweb.app.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;

import com.onnorokomweb.app.entity.Department;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "profile")
public class Profile{

    public enum Status {TRIAL, PROVISION, PERMANENT, INTERN, ONVACATION, LEFT, PARTTIME}

    public enum Sex {MALE, FEMALE}

    private int id;
    private String nickName;
    private String fullName;
    private String email;
    private String phone;
    private Sex sex;
    private String address;
    private String pinNumber;
    private String designation;
    private Department department;
    private String education;
    private Date joinDate;
    private Date permanentDate;
    private Status status;
    private String image;
    private String coverImage;
    private Date birthday;
    private String biography;
    private String aboutMe;
    private String hobby;
    private Timestamp created;
    private Timestamp updated;

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "sex")
    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "department_id")
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }


    @NotBlank
    @Column(name = "nick_name")
    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @NotBlank
    @Column(name = "full_name")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @NotBlank
    @Email
    @Column(unique = true)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NotBlank
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @NotBlank
    @Column(name = "address")
    @Length(max = 255)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @NotBlank
    @Column(name = "pin_number")
    @Length(max = 10)
    public String getPinNumber() {
        return pinNumber;
    }

    public void setPinNumber(String pinNumber) {
        this.pinNumber = pinNumber;
    }

    @NotBlank
    @Column(name = "designation")
    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @NotBlank
    @Column(name = "education")
    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    @NotNull
    @Column(name = "join_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public Date getJoinDate() {
        return joinDate;
    }


    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    @Column(name = "permanent_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public Date getPermanentDate() {
        return permanentDate;
    }

    public void setPermanentDate(Date permanentDate) {
        this.permanentDate = permanentDate;
    }

    @Column(name = "image")
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Column(name = "cover_image")
    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    @NotNull
    @Column(name = "birthday")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @NotBlank
    @Column(name = "biography")
    @Length(max = 2048)
    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    @NotBlank
    @Column(name = "about_me")
    @Length(max = 5120)
    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    @Column(name = "hobby")
    @Length(max = 512)
    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Column(name = "updated")
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @PrePersist
    public void onCreate() {
        created = new Timestamp(new Date().getTime());
    }

    @PreUpdate
    public void onUpdate() {
        updated = new Timestamp(new Date().getTime());
    }


    @Override
    public String toString() {
        return "Profile{" +
            ", id=" + id +
            ", nickName='" + nickName + '\'' +
            ", fullName='" + fullName + '\'' +
            ", email='" + email + '\'' +
            ", phone='" + phone + '\'' +
            ", sex=" + sex +
            ", address='" + address + '\'' +
            ", pinNumber='" + pinNumber + '\'' +
            ", designation='" + designation + '\'' +
            ", department='" + department + '\'' +
            ", education='" + education + '\'' +
            ", joinDate=" + joinDate +
            ", permanentDate=" + permanentDate +
            ", jobStatus=" + status +
            ", image='" + image + '\'' +
            ", coverImage='" + coverImage + '\'' +
            ", birthday=" + birthday +
            ", biography='" + biography + '\'' +
            ", aboutMe='" + aboutMe + '\'' +
            ", created=" + created +
            ", updated=" + updated +
            '}';
    }
}
