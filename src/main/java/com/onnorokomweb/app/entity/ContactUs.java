package com.onnorokomweb.app.entity;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;


@Entity
@Table(name="contact_us")
public class ContactUs {

    private int id;
    private String name;
    private String email;
    private String message;
    private String phone;
    private String status;
    private Timestamp created;
    private Timestamp updated;

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true)
    private int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotBlank
    @Column(name="name")
    @Length(max =50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotBlank
    @Column(name="email")
    @Length(max =50)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NotBlank
    @Column(name="message")
    @Length(max =50)
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Column(name="phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name="status")
    @Length(max =50)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Column(name = "updated")
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @PrePersist
    public void onCreate() {
        created = new Timestamp(new Date().getTime());
    }

    @PreUpdate
    public void onUpdate() {
        updated = new Timestamp(new Date().getTime());
    }

    @Override
    public String  toString()
    {
        return "ContactUs{" +  "id=" + id +
            ", name='" + name + '\'' +
            ", email='" + email + '\'' +
            ", message='" + message + '\'' +
            ", status='" + status + '\'' +
            '}';
    }

}
