package com.onnorokomweb.app.entity.common;

import com.onnorokomweb.app.entity.Role;
import com.onnorokomweb.app.entity.User;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

public class CurrentUser extends org.springframework.security.core.userdetails.User {

    private User user;

    public CurrentUser(User user, String[] roleNameList) {
        super(user.getEmail(), user.getPassword(), user.getStatus().equals(User.Status.ACTIVE), true, true, true, AuthorityUtils.createAuthorityList(roleNameList));

        this.user = user;
    }

    private User getUser() {
        return user;
    }

    private List<Role> getRole() {
        return user.getRoles();
    }

    private int getId() {
        return user.getId();
    }

    @Override
    public String toString() {
        return "CurrentUser{" +
            "user=" + user +
            '}';
    }
}
