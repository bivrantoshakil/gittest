package com.onnorokomweb.app.entity;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(name = "user")
public class User{

    public enum Status { PENDING, ACTIVE, INACTIVE }
    private int id;
    private String email;
    private Status status;
    private String password;
    private Profile profile;
    private Timestamp created;
    private Timestamp updated;
    private List<Role> roles;

    public User() {
        this.status = Status.PENDING;
        this.created=new Timestamp(new Date().getTime());
    }

    @Id
    @Column(name = "id", unique=true, nullable=false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotBlank
    @Column(name = "email", unique = true)
    @Length(max = 255)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @NotBlank
    @Column(name = "password")
    @Length(max = 255)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @OneToOne
    @JoinColumn(name = "id",insertable = false, updatable = false)
    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
        if (profile != null) {
            setId(profile.getId());
        }
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Column(name = "updated")
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = {@JoinColumn(name = "user_id", insertable = false, updatable = false)}, inverseJoinColumns = {@JoinColumn(name = "role_id", insertable = false, updatable = false)})
    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @PrePersist
    public void onCreate() {
        created = new Timestamp(new Date().getTime());
    }

    @PreUpdate
    public void onUpdate() {
        updated = new Timestamp(new Date().getTime());
    }

    @Override
    public String toString() {
        return "User{" +
            "id=" + id +
            ", email='" + email + '\'' +
            ", status='" + status + '\'' +
            ", password='" + password + '\'' +
            ", created=" + created +
            ", updated=" + updated +
            ", roles=" + roles +
            '}';
    }

}
