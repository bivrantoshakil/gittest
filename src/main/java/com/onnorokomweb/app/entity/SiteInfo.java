package com.onnorokomweb.app.entity;

import org.hibernate.validator.constraints.Length;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;


@Entity
@Table(name = "site_info")
public class SiteInfo {

    private int id;
    private String title;
    private String titleBn;
    private String content;
    private String contentBn;
    private Timestamp created;
    private Timestamp updated;

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotNull
    @Column(name = "title", unique = true)
    @Length(max = 256)
    public String getTitle() {
        return title;
    }

    public void setTitle(String keys) {
        this.title = keys;
    }

    @Column(name = "title_bn")
    @Length(max = 256)
    public String getTitleBn() {
        return titleBn;
    }

    public void setTitleBn(String titleBn) {
        this.titleBn = titleBn;
    }

    @Column(name = "content")
    @Length(max = 5120)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "content_bn")
    @Length(max = 5120)
    public String getContentBn() {
        return contentBn;
    }

    public void setContentBn(String contentBn) {
        this.contentBn = contentBn;
    }

    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Column(name = "updated")
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @PrePersist
    public void onCreate() {
        created = new Timestamp(new Date().getTime());
    }

    @PreUpdate
    public void onUpdate() {
        updated = new Timestamp(new Date().getTime());
    }

    @Override
    public String toString() {
        return "SiteInfo{" +
            "id=" + id +
            ", title='" + title + '\'' +
            ", titleBn='" + titleBn + '\'' +
            ", content='" + content + '\'' +
            '}';
    }
}
