package com.onnorokomweb.app.repository;


import com.onnorokomweb.app.entity.Profile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProfileRepository extends CrudRepository<Profile , Integer> {

    Profile findById(int id);

    Profile findByEmail(String email);

    @Query("from Profile order by nickName asc")
    List<Profile> findAllOrderByNickName();

    @Query("select profile from Profile profile where profile.status!='LEFT' and profile.status!='ONVACATION' order by profile.nickName asc")
    List<Profile> findAllWhereStatus();

    @Query("select profile from Profile profile join profile.department dept where profile.status!='LEFT' and profile.status!='ONVACATION' and dept.id = ?1 order by profile.nickName asc ")
    List<Profile> findAllStatusAndDept(int id);


}
