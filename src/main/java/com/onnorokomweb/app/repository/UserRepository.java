package com.onnorokomweb.app.repository;

import com.onnorokomweb.app.entity.User;
import org.springframework.data.repository.CrudRepository;


public interface UserRepository extends CrudRepository<User, Integer> {

    User findById(int id);
    User findByEmail(String email);
}
