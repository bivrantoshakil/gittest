package com.onnorokomweb.app.repository;

import com.onnorokomweb.app.entity.Role;
import org.springframework.data.repository.CrudRepository;


public interface RoleRepository extends CrudRepository<Role, Integer> {

    Role findById(int id);


}
