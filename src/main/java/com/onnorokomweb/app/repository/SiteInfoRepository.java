package com.onnorokomweb.app.repository;

import com.onnorokomweb.app.entity.SiteInfo;
import org.springframework.data.repository.CrudRepository;


public interface SiteInfoRepository extends CrudRepository<SiteInfo, Integer> {

    SiteInfo findById(int id);

}
