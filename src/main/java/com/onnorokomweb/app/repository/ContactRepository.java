package com.onnorokomweb.app.repository;

import com.onnorokomweb.app.entity.ContactUs;
import org.springframework.data.repository.CrudRepository;


public interface ContactRepository extends CrudRepository<ContactUs, Integer> {

}
