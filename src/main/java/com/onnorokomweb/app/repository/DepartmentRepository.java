package com.onnorokomweb.app.repository;

import com.onnorokomweb.app.entity.Department;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import java.util.List;


public interface DepartmentRepository extends CrudRepository<Department, Integer>{
    Department findById(int id);
    @Query("from Department dept where dept.status ='ACTIVE' order by name asc")
    List<Department> findAllOrderByName();
}
