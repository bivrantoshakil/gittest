package com.onnorokomweb.app.validation;

import com.onnorokomweb.app.entity.Profile;
import com.onnorokomweb.app.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ProfileSaveValidator  implements Validator {

    @Autowired
    private ProfileService profileService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(Profile.class);
    }

    @Override
    public void validate(Object object, Errors errors) {
        Profile profileNew, profileOld;
        try{
            profileNew = (Profile)object;
            profileOld= profileService.getById(profileNew.getId());

            if(null == profileOld || (null != profileOld && !profileOld.getEmail().equalsIgnoreCase(profileNew.getEmail()))) {
                validateEmail(errors, profileNew);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void validateEmail(Errors errors, Profile profile) {
        if(profileService.getByEmail(profile.getEmail()) != null) {
            errors.rejectValue("email", "email.exists", "Profile with this email address already exists");
        }
    }


}
