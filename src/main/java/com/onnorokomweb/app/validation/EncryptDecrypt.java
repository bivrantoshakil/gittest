package com.onnorokomweb.app.validation;

import org.springframework.stereotype.Component;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import java.security.spec.KeySpec;

@Component(value = "encryptdecrypt")
public class EncryptDecrypt {

    public static final String UNICODE_FORMAT = "UTF-8";
	public static final String DESEDE_ENCRIPTION_SCHEME = "DESede";
	private KeySpec myKeySpec;
	private SecretKeyFactory mySecFac;
	private Cipher cipher;
	byte[] keyBytes;
	private String myEncKey;
	private String myEncScheme;
	SecretKey key;

	public EncryptDecrypt() throws Exception{
		myEncKey = "Rokomari@Secret!EncryptionKey^@98";
		myEncScheme = DESEDE_ENCRIPTION_SCHEME;
		keyBytes = myEncKey.getBytes(UNICODE_FORMAT);
		myKeySpec = new DESedeKeySpec(keyBytes);
		mySecFac = SecretKeyFactory.getInstance(myEncScheme);
		cipher = Cipher.getInstance(myEncScheme);
		key = mySecFac.generateSecret(myKeySpec);
	}

	public String getEncryptValue(String unEncValue ){
		String encString = null;
		try{
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plainTxt = unEncValue.getBytes(UNICODE_FORMAT);
			byte[] encryptedTxt = cipher.doFinal(plainTxt);
			BASE64Encoder base64encoder = new BASE64Encoder();
			encString = base64encoder.encode(encryptedTxt);
		} catch(Exception e){
			e.printStackTrace();
		}

        return encString;
	}

	public String getDecryptValue(String encrypted){
		String decString = null;
		try{
			cipher.init(Cipher.DECRYPT_MODE, key);
			BASE64Decoder base64decoder = new BASE64Decoder();
			byte[] encText = base64decoder.decodeBuffer(encrypted);
			byte[] original = cipher.doFinal( encText );
			decString = bytesToString(original);
		}catch(Exception e){
			e.printStackTrace();
		}
        return decString;
	}
	public static String bytesToString(byte[] bytes){
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<bytes.length; i++){
			sb.append((char)bytes[i]);
		}
		return sb.toString();
	}

}
