-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 29, 2015 at 07:29 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `onnorokomwebdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE IF NOT EXISTS `contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `message` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `created`, `email`, `message`, `name`, `phone`, `status`, `updated`) VALUES
(1, '2015-06-22 10:19:19', 'jakaria.ict@gmail.com', 'good', 'Rokomari', NULL, '0', NULL),
(2, '2015-06-22 10:19:32', 'jakaria@rokomari.com', 'sadd', 'Rokomari', NULL, '0', NULL),
(3, '2015-06-24 09:42:00', 'jakaria.ict@gmail.com', 'good', 'Rokomari', NULL, '0', NULL),
(4, '2015-06-24 11:05:15', 'jakaria.ict@gmail.com', 'good', 'Rokomari', NULL, '0', NULL),
(5, '2015-06-24 11:05:28', 'jakaria.ict@gmail.com', 'good', 'Rokomari', NULL, '0', NULL),
(6, '2015-06-24 11:07:30', 'jakaria.ict@gmail.com', 'good', 'Rokomari', NULL, '0', NULL),
(7, '2015-06-25 09:48:00', 'shakil@rokomari.com', 'asdasd', 'Asdasd', NULL, '0', NULL),
(8, '2015-06-25 09:49:21', 'jakaria.ict@gmail.com', 'good', 'Rokomari', NULL, '0', NULL),
(9, '2015-06-25 17:16:48', 'jakaria.ict@gmail.com', 'good', 'Rokomari', NULL, '0', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `description` longtext,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(56) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `image1` varchar(255) DEFAULT NULL,
  `image2` varchar(255) DEFAULT NULL,
  `image3` varchar(255) DEFAULT NULL,
  `image4` varchar(255) DEFAULT NULL,
  `image5` varchar(255) DEFAULT NULL,
  `image1_min` varchar(255) DEFAULT NULL,
  `image2_min` varchar(255) DEFAULT NULL,
  `image3_min` varchar(255) DEFAULT NULL,
  `image4_min` varchar(255) DEFAULT NULL,
  `image5_min` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `created`, `description`, `image`, `name`, `updated`, `image1`, `image2`, `image3`, `image4`, `image5`, `image1_min`, `image2_min`, `image3_min`, `image4_min`, `image5_min`) VALUES
(1, '2015-06-18 16:56:32', 'রকমারির তথ্যভান্ডার। রকমারির সকল ডাটা এনলিস্টিং,ম্যানেজিং,এনালাইজিং,প্রভৃতি কাজে এই বিভাগ সদাব্যস্ত। বইয়ের এন্ট্রি থেকে শুরু করে ডাটা স্প্রেডশিট,কম্পিউটারের টুকটাক শব্দের মূর্ছনায় প্রতিনিয়ত এই বিভাগের দ্বারাই সমৃদ্ধ হচ্ছে রকমারি।', 'Data Management (DM).jpg', 'ডাটা ম্যানেজমেন্ট', '2015-06-23 15:46:54', 'datamanagement(dm)1.jpg', 'datamanagement(dm)2.jpg', 'datamanagement(dm)3.jpg', 'datamanagement(dm)4.jpg', 'datamanagement(dm)5.jpg', NULL, NULL, NULL, NULL, NULL),
(2, '2015-06-18 16:57:12', 'রকমারির পণ্যসুবিধা আসে কোথা থেকে? পণ্যের নিয়মিত আনায়ন নিশ্চিতকরণ ও পণ্যমালিকদের সাথে সুসম্পর্ক বজায় রাখাই মার্চেন্ট ম্যানেজমেন্টের কাজ। কেবলমাত্র ক্রেতাদের সাথেই নয় এই তিন বছরে রকমারি প্রকাশক,ব্যবসায়ী,উদ্যোক্তাদের সাথেও চমৎকার সম্পর্ক স্থাপন করেছে। আর এই সুসম্পর্ক সৃষ্টির পুরো অবদানটিই এই বিভাগের।', 'Merchant Management (MM).jpg', 'মার্চেন্ট অ্যান্ড প্রোডাক্ট ম্যানেজমেন্ট', '2015-06-23 15:44:35', 'merchantmanagement(mm)1.jpg', 'merchantmanagement(mm)2.jpg', 'merchantmanagement(mm)3.jpg', 'merchantmanagement(mm)4.jpg', 'merchantmanagement(mm)5.jpg', NULL, NULL, NULL, NULL, NULL),
(3, '2015-06-18 16:57:35', 'প্রোডাক্ট ম্যানেজমেন্ট', 'Product Management (PM).jpg', 'প্রোডাক্ট ম্যানেজমেন্ট', '2015-06-23 15:45:02', 'productmanagement(pm)1.jpg', 'productmanagement(pm)2.jpg', 'productmanagement(pm)3.jpg', 'productmanagement(pm)4.jpg', 'productmanagement(pm)5.jpg', NULL, NULL, NULL, NULL, NULL),
(4, '2015-06-18 16:58:01', 'রকমারির কণ্ঠ কাস্টমার কেয়ার। রকমারির ব্যস্ততম এই বিভাগটির পাশ দিয়ে হেঁটে গেলে সারাটি ক্ষণ টেলিফোনের রিং আর “হ্যালো...রকমারি ডট কম থেকে বলছি” শোনা যায়। পাঠকের সাথে সরাসরি টেলিফোনে ইন্টার‍্যাকশন করা ও রকমারির পণ্যসুবিধার কথা পাঠকের কাছে পৌঁছে দেয়ায় কাস্টমার কেয়ার দক্ষতার পরিচয় দিয়ে চলছে। বই অনুরোধ গ্রহণ,টেলি মার্কেটিং,ক্রয় সমস্যার সমাধানসহ আরও অনেক কাজে কাজী রকমারি কাস্টমার কেয়ার।', 'Customer Management (CM).jpg', 'কাস্টমার কেয়ার', '2015-06-23 15:40:52', 'customermanagement(cm)1.jpg', 'customermanagement(cm)2.jpg', 'customermanagement(cm)3.jpg', 'customermanagement(cm)4.jpg', 'customermanagement(cm)5.jpg', NULL, NULL, NULL, NULL, NULL),
(5, '2015-06-18 16:58:41', 'রকমারির সবচেয়ে কর্মচঞ্চল ও সর্ববৃহৎ পরিসরের বিভাগ। এই বিভাগের বহুমুখী কাজ ও সদস্যদের ক্লান্তিহীন ডেডিকেশন এই বিভাগটিকে অনন্য করে তুলেছে। পণ্য সংগ্রহ,মজুদ,প্যাকেজিং ও সরবরাহ প্রতিটি কাজেই এই বিভাগের পারদর্শিতার ছাপ স্পষ্ট। রকমারির পিলার সদৃশ এই সাপ্লাই চেইন ম্যানেজমেন্ট ও তার সদস্যরা কিংবা সুখ হরকরারা।', 'Supply Chain Management (SCM).jpg', 'সাপ্লাই চেইন ম্যানেজমেন্ট', '2015-06-23 15:46:18', 'supplychainmanagement(scm)1.jpg', 'supplychainmanagement(scm)2.jpg', 'supplychainmanagement(scm)3.jpg', 'supplychainmanagement(scm)4.jpg', 'supplychainmanagement(scm)5.jpg', NULL, NULL, NULL, NULL, NULL),
(6, '2015-06-18 16:59:25', 'রকমারির সবচাইতে জনশূন্য অঞ্চল। যদিও মাসের প্রথম দিকে বেশ জনবহুল থাকে। রকমারির সবচাইতে সুখী স্থানও বলা যায়। কারণ মাসের প্রথমে এখান থেকে বের হওয়া প্রতিটি মানুষের মুখেই হাসি লেগে থাকে। রকমারি ডট কমের যাবতীয় আর্থিক লেনদেন,ফিন্যান্শিয়াল ইস্যু এখান থেকেই পরিচালনা হয়। ', 'Account & Finance (AF).jpg', 'একাউন্টস অ্যান্ড ফিন্যান্স', '2015-06-25 12:32:19', 'account&finance(af)1.jpg', 'account&finance(af)2.jpg', 'account&finance(af)3.jpg', 'account&finance(af)4.jpg', 'account&finance(af)5.jpg', NULL, NULL, NULL, NULL, NULL),
(7, '2015-06-18 17:00:13', 'রকমারির অন্যতম সৃষ্টিশীল বিভাগ। পণ্য বিক্রয়ের জন্য আইডিয়া প্রদান ও ইমপ্লিমেন্টের কাজ দুটোই এই বিভাগ থেকে সম্পাদিত হয়। রকমারির কন্টেন্ট ডেভেলপমেন্ট,পণ্যের ব্র্যান্ডভ্যালু বৃদ্ধিকরণ,মার্কেটিং স্ট্রাটেজি,ইত্যাদি কাজ করে চলছে এই বিভাগের চিন্তক মানুষেরা।৩ বছরে এক লাখেরও বেশি ক্রেতাকে রকমারির সাথে সংযুক্ত করার জন্য এই বিভাগের অবদান সবচেয়ে বেশী।', 'Marketing & Branding (MB).jpg', 'মার্কেটিং অ্যান্ড ব্র্যান্ডিং', '2015-06-23 15:43:54', 'marketing&branding(mb)1.jpg', 'marketing&branding(mb)2.jpg', 'marketing&branding(mb)3.jpg', 'marketing&branding(mb)4.jpg', 'marketing&branding(mb)5.jpg', NULL, NULL, NULL, NULL, NULL),
(8, '2015-06-18 17:00:41', 'অনলাইন ভিত্তিক প্রতিষ্ঠান হওয়া সত্ত্বেও রকমারি সরাসরি সাধারণ মানুষের সাথে সুসম্পর্ক গঠনে শুরু থেকেই সচেষ্ট ছিল। তাই ফিজিক্যালি বিভিন্ন ইভেন্ট করা,প্রেস রিলিজ,অনলাইন-অফলাইন মেলা,প্রভৃতি দায়িত্ব পাবলিক রিলেশন বিভাগ দক্ষতার সাথে পালন করছে।', 'Public Relation (PR).jpg', 'পাবলিক রিলেশন', '2015-06-23 15:45:36', 'publicrelation(pr)1.jpg', 'publicrelation(pr)2.jpg', 'publicrelation(pr)3.jpg', 'publicrelation(pr)4.jpg', 'publicrelation(pr)5.jpg', NULL, NULL, NULL, NULL, NULL),
(9, '2015-06-18 17:01:08', 'বাংলাদেশের অন্যতম সহজবোধ্য ও সুদৃশ্য ওয়েবসাইট হচ্ছে রকমারি ডট কমের ওয়েবসাইট। খুব সহজেই যেন ক্রেতা এই সাইটে এসে তার প্রয়োজনীয় পণ্য ক্রয় করতে পারে এই ব্যাপারে রকমারির সফটওয়্যার ইঞ্জিনিয়ারিং বিভাগের সতর্ক দৃষ্টি থাকে সর্বদা। খুব শীঘ্রই নতুন রুপে আরও সুদৃশ্য ও আরও ইউজার ফ্রেন্ডলি সাইট ও রকমারি মোবাইল অ্যাপ নিয়ে আসছে রকমারির এই ক্রিয়েটিভ বিভাগ।', 'Software Engineering (SE).jpg', 'সফটওয়্যার ইঞ্জিনিয়ারিং', '2015-06-23 15:46:12', 'softwareengineering(se)1.jpg', 'softwareengineering(se)2.jpg', 'softwareengineering(se)3.jpg', 'softwareengineering(se)4.jpg', 'softwareengineering(se)5.jpg', NULL, NULL, NULL, NULL, NULL),
(10, '2015-06-18 17:01:46', 'রকমারির দখিনা দুয়ার। চমৎকার সব সৃষ্টিশীলতা দিয়ে এই বিভাগ এখন রকমারির মুখে পরিণত হয়েছে। প্রোডাক্ট ফটোগ্রাফি থেকে শুরু করে ব্যানার-ফেস্টুন,অনলাইন-পত্রিকা অ্যাড,ইলাস্ট্রেশন সবক্ষেত্রেই এই বিভাগের অবদান রয়েছে।', 'Graphics & Creative (GC).jpg', 'গ্রাফিক্স অ্যান্ড ক্রিয়েটিভ', '2015-06-23 15:42:28', 'graphics&creative(gc)1.jpg', 'graphics&creative(gc)2.jpg', 'graphics&creative(gc)3.jpg', 'graphics&creative(gc)4.jpg', 'graphics&creative(gc)5.jpg', NULL, NULL, NULL, NULL, NULL),
(11, '2015-06-18 17:02:14', 'বলা যায় “ওয়ান ম্যান আর্মি” কিন্তু কাজের ক্ষেত্রে বহুমুখী। রকমারি একটি সুখী পরিবার- আর এই পরিবারের দেখভাল করার দায়িত্ব এই বিভাগটিরই। পরিবারের সদস্যদের বিশ্বস্ততার আশ্বাস দেয়া,নতুন সদস্য নিয়োগ,নতুন সদস্যদের প্রশিক্ষণ দেয়া এবং প্রতিষ্ঠানের সাংস্কৃতিক ও কাঠামোগত উন্নয়ন,ইত্যাদি সকল বিষয় মানব সম্পদ বিভাগের দায়িত্ব।', 'Human Resource (HR).jpg', 'হিউম্যান রিসোর্স', '2015-06-23 15:43:02', 'humanresource(hr)1.jpg', 'humanresource(hr)2.jpg', 'humanresource(hr)3.jpg', 'humanresource(hr)4.jpg', 'humanresource(hr)5.jpg', NULL, NULL, NULL, NULL, NULL),
(12, '2015-06-18 17:20:43', 'রকমারির আরেকটি “ওয়ান ম্যান আর্মি” বিভাগ। একটি ই-কমার্স প্রতিষ্ঠানের অন্যতম গুরুত্বপূর্ণ বিভাগ হচ্ছে আইটি অ্যান্ড সাপোর্ট। ওয়াইফাই নেটওয়ার্কিং,এফটিপি সার্ভার নেটওয়ার্কিং,কলসেন্টার নেটওয়ার্কিং,ব্যান্ডউইথ ম্যানেজমেন্ট,ইত্যাদি কাজ সামাল দিয়ে রকমারিকে সার্বক্ষণিক নেটওয়ার্কিং এর ভেতর রেখে বিক্রয়সেবা নিশ্চিত করে আসছে এই বিভাগ।', 'IT and Support (IS).jpg', 'আইটি অ্যান্ড সাপোর্ট', '2015-06-23 15:43:24', 'itandsupport(is)1.jpg', 'itandsupport(is)2.jpg', 'itandsupport(is)3.jpg', 'itandsupport(is)4.jpg', 'itandsupport(is)5.jpg', NULL, NULL, NULL, NULL, NULL),
(13, '2015-06-18 18:49:50', '“সরব” এর প্রতিশব্দ বলা যায় এই বিভাগটিকে। সদা হাস্যোজ্জ্বল,সরবতায় মুখোর এই বিভাগ থেকেই রকমারির সকল বাণিজ্যিক কার্যক্রম পরিচালিত হয়। রকমারির উত্তরোত্তর উন্নতিতে এই বিশাল বিভাগের অবদান অনস্বীকার্য।', 'Business Development (BD).jpg', 'বিজনেস ডেভেলপমেন্ট ', '2015-06-23 15:46:50', 'businessdevelopment(bd)1.jpg', 'businessdevelopment(bd)2.jpg', 'businessdevelopment(bd)3.jpg', 'businessdevelopment(bd)4.jpg', 'businessdevelopment(bd)5.jpg', NULL, NULL, NULL, NULL, NULL),
(14, '2015-06-18 18:51:52', 'It’s not only a parcel but also a happiness. রকমারি ডট কম স্বল্পতম সময়ের মাঝে ক্রেতা/পাঠককে ক্রয়কৃত হ্যাপিনেস (পার্সেল) পৌঁছে দিতে সদা সচেষ্ট। তাই রকমারি ডেভেলপ করেছে নিজস্ব ডেলিভারি সিস্টেম। ঢাকা মহানগরীর যেকোনো জায়গায় তাই অর্ডার করার পর খুব অল্প সময়েই পৌঁছে যায় “পার্সেল অফ হ্যাপিনেস”। এছাড়া সারাদেশের সকল পাঠক/ক্রেতাদের অর্ডার করা পার্সেল পৌঁছে দিতে রকমারি ডট কম ডাকবিভাগ,কুরিয়ার সার্ভিসদের সাথেও খুব ভালো সম্পর্ক বজায় রেখে চলছে। খুব শীঘ্রই সমগ্র বাংলাদেশ জুড়ে রকমারি ডট কমের নিজস্ব ডেলিভারি সিস্টেম থাকবে বলে আশা করা যায়।', 'Rokomari Courier (RC).jpg', 'রকমারি কুরিয়ার', '2015-06-23 15:46:03', 'rokomaricourier(rc)1.jpg', 'rokomaricourier(rc)2.jpg', 'rokomaricourier(rc)3.jpg', 'rokomaricourier(rc)4.jpg', 'rokomaricourier(rc)5.jpg', NULL, NULL, NULL, NULL, NULL),
(15, '2015-06-24 16:21:57', 'অপারেশান ম্যানেজমেন্ট', NULL, 'অপারেশান ম্যানেজমেন্ট', NULL, 'operationmanagement(om)1.jpg', 'operationmanagement(om)2.jpg', 'operationmanagement(om)3.jpg', 'operationmanagement(om)4.jpg', 'operationmanagement(om)5.jpg', NULL, NULL, NULL, NULL, NULL),
(16, '2015-06-24 16:25:00', 'চলুন ফিরে যাই আমাদের ফেলে আসা দিনে, পারিবারিক প্রতিবন্ধকতাকে জয় করেই হয়তো আপনারই কোন বন্ধু কিংবা পরিচিত কেউ স্বপ্ন দেখেছিলো ক্রিকেটার হওয়ার, হয়তো যার প্রতি ডেলিভারিতে স্বপ্ন থাকতো ম্যাকগ্রা কিংবা ওয়াসিম আকরাম হওয়ার, হয়তো প্রতিটি শট এ ভাবতো শচীনকে ছাড়িয়ে যাওয়ার কিংবা জন্টি রোদস এর মত উড়ে গিয়ে কোন বল ধরেই হয়তো হাসতো তৃপ্তির হাসি, আর সেই হাসি ছাপিয়ে যেতো হাত কিংবা পায়ের চামড়া ছিলে যাওয়ার যন্ত্রণা । আর হয়তো তার চোখে আপনিও স্বপ্ন দেখেছিলেন সে নিশ্চয়ই কোন একদিন তার স্বপ্নের খেলোয়াড় হবে কিন্তু সময়ের নির্মমতায় সেই স্বপ্নগুলোর ঠাই হয়েছে আমাদের স্মৃতিতে, হয়তোবা কোন এক মুদি দোকানে, কোন এক কর্পোরেট অফিসে কিংবা কোন এক ধুলোপড়া উঠোনে। ', NULL, 'টিম রকমারি', '2015-06-25 15:35:05', 'teamrokomari(tr)1.jpg', 'teamrokomari(tr)2.jpg', 'teamrokomari(tr)3.jpg', 'teamrokomari(tr)4.jpg', 'teamrokomari(tr)5.jpg', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `about_me` longtext NOT NULL,
  `address` varchar(255) NOT NULL,
  `biography` longtext NOT NULL,
  `birthday` date NOT NULL,
  `cover_image` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `designation` varchar(255) NOT NULL,
  `education` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `join_date` date NOT NULL,
  `nick_name` varchar(255) NOT NULL,
  `permanent_date` date DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `pin_number` varchar(10) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `hobby` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_9d5dpsf2ufa6rjbi3y0elkdcd` (`email`),
  KEY `FK_ak32q581e33b0ouqitj0i07b1` (`department_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=115 ;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `about_me`, `address`, `biography`, `birthday`, `cover_image`, `created`, `designation`, `education`, `email`, `full_name`, `image`, `join_date`, `nick_name`, `permanent_date`, `phone`, `pin_number`, `sex`, `status`, `updated`, `department_id`, `hobby`) VALUES
(2, 'N/A', '669/2, 2nd Floor, East Dholaipar, Jatrabari, Dhaka.', 'N/A', '1979-03-01', NULL, '2015-06-18 17:15:40', 'Team Member', 'Hons', 'jahid@rokomari.com', 'Md. Jhahidur Rahman', 'jahid@rokomari.com.jpg', '2014-03-22', 'Jahid', '2014-10-01', '01917009119', '222', 'MALE', 'PERMANENT', '2015-06-24 16:09:34', 9, NULL),
(3, 'NA', '2F/4, Extended Pallabi, Mirpur, Dhaka- 1216.', 'NA', '1990-12-10', NULL, '2015-06-18 17:26:34', 'Team Member', 'NA', 'kabir@rokomari.com', 'Md. Humayan Kabir', 'kabir@rokomari.com.jpg', '2013-06-04', 'Kabir', '2014-01-01', '01833168110', '163', 'MALE', 'PERMANENT', '2015-06-24 16:13:11', 12, NULL),
(4, 'NA', 'Lota Nibash (Near Hujr Bari), Sayedabad, Dhaka.', 'NA', '1983-11-02', NULL, '2015-06-18 17:32:22', 'Team Member', 'NA', 'malak@rokomari.com', 'Abdul Malak', 'malak@rokomari.com.jpg', '2011-03-10', 'Malak', '2011-06-01', '01833168183', '1001', 'MALE', 'PERMANENT', '2015-06-29 10:31:50', 5, ''),
(5, 'NA', 'Raselder Bari, Jhaulahati, Jhauchor Bazar, Hazaribag, Dhaka.\r\n', 'NA', '1983-09-06', NULL, '2015-06-18 17:41:21', 'Team Member', 'NA', 'tipu@rokomari.com', 'Md. Saidul Islam Tipu', 'tipu@rokomari.com.jpg', '2009-01-01', 'Tipu', '2009-07-01', '01729744474', '1002', 'MALE', 'PERMANENT', '2015-06-24 17:20:40', 5, NULL),
(6, 'NA', '2/2E, Eden Complex, Arambag, Motijheel, Dhaka- 1000\r\n\r\n', 'NA', '1992-02-05', NULL, '2015-06-18 17:46:07', 'Team Member', 'NA', 'masum@rokomari.com', 'Md. Masum Rana', 'masum@rokomari.com.jpg', '2012-11-12', 'Masum', '2013-04-12', '01847103135', '1018', 'MALE', 'PERMANENT', '2015-06-29 11:36:10', 1, ''),
(7, 'NA', '2/2E, Eden Complex, Arambag, Motijheel, Dhaka- 1000\r\n\r\n', 'NA', '1986-10-30', NULL, '2015-06-18 17:47:59', 'Team Member', 'NA', 'bonie@rokomari.com', 'Shahida Sharin Bonie', 'bonie@rokomari.com.jpg', '2012-11-12', 'Bonie', '2013-01-01', '01766388109', '1022', 'FEMALE', 'PERMANENT', NULL, 4, NULL),
(8, 'NA', 'South Komolapur, Motijhil, Dhaka.\r\n\r\n\r\n', 'NA', '1990-10-29', NULL, '2015-06-18 17:53:34', 'Team Member', 'NA', 'jimy@rokomari.com', 'Md. Baki Billah Jimy', 'jimy@rokomari.com.jpg', '2012-02-01', 'Jimy', '2012-07-01', '01833168184', '1026', 'MALE', 'PERMANENT', '2015-06-24 16:40:33', 2, NULL),
(9, 'NA', 'House no- 21, Road no- 5, Block- B, Bonosri, Rampura, Dhaka.\r\n', 'NA', '1987-04-15', NULL, '2015-06-18 17:55:29', 'Team Member', 'NA', 'sweet@rokomari.com', 'Kazi Kawser Sweet', 'sweet@rokomari.com.jpg', '2014-11-14', 'Sweet', '2013-05-01', '01833168194', '1028', 'MALE', 'PERMANENT', '2015-06-24 16:40:24', 2, NULL),
(10, 'NA', 'Road No- 5, House No- 2, Hirajhil, Chittagong Road, Siddhirganj, Narayanganj.\r\n\r\n', 'NA', '1987-07-04', NULL, '2015-06-18 17:57:57', 'Team Member', 'NA', 'faruk@rokomari.com', 'Md. Faruk Mia', 'faruk@rokomari.com.jpg', '2013-02-12', 'Faruk', '2013-09-01', '01833168193', '1029', 'MALE', 'PERMANENT', '2015-06-24 16:40:44', 2, NULL),
(11, 'NA', 'Road- 6, House- 7, Borobag, Mirpur- 2, Dhaka.\r\n\r\n\r\n', 'NA', '1986-07-14', NULL, '2015-06-18 18:00:04', 'Team Member', 'NA', 'jahurul@rokomari.com', 'Mirza Jahurul Islam Miraj', 'jahurul@rokomari.com.jpg', '2012-02-06', 'Miraj', '2012-05-01', '01841332333', '1031', 'MALE', 'PERMANENT', '2015-06-24 16:29:25', 6, NULL),
(12, 'NA', 'Mirpur, Dhaka\r\n\r\n\r\n', 'NA', '1991-01-17', NULL, '2015-06-18 18:02:55', 'Team Member', 'NA', 'rana@rokomari.com', 'Mehedi Hasan Rana', 'rana@rokomari.com.jpg', '2012-09-01', 'Rana', '2013-01-01', '01833168190', '1034', 'MALE', 'PERMANENT', '2015-06-24 16:33:34', 7, NULL),
(13, 'NA', 'Mohammadpur, Dhaka\r\n\r\n\r\n', 'NA', '1990-07-12', NULL, '2015-06-18 18:05:38', 'Team Member', 'NA', 'adnan@rokomari.com', 'Md. Adnan Khan', 'adnan@rokomari.com.jpg', '2008-07-07', 'Adnan', '2009-01-01', '01841188844', '1043', 'MALE', 'PERMANENT', '2015-06-24 16:35:30', 13, NULL),
(14, 'NA', '74/5, Greenroad, Firmgate, Dhaka.\r\n', 'NA', '1988-12-12', NULL, '2015-06-18 18:07:37', 'Team Member', 'NA', 'sadi@rokomari.com', 'Mahmudl Hasan Sadi', 'sadi@rokomari.com.jpg', '2013-12-12', 'Sadi', '2014-09-01', '01833168187', '1053', 'MALE', 'PERMANENT', '2015-06-24 16:14:05', 7, NULL),
(15, 'NA', '149/6, Arambag, Motijheel, Dhaka-1200.\r\n', 'NA', '1980-12-16', NULL, '2015-06-18 18:09:42', 'Team Member', 'NA', 'bahar@rokomari.com', 'Bahar Uddin', 'bahar@rokomari.com.jpg', '2014-03-01', 'Bahar', '2014-10-01', '01714492028', '1057', 'MALE', 'PERMANENT', '2015-06-24 16:43:01', 14, NULL),
(16, 'NA', '253/4. Sultanganj, Rayer Bazar, Dhaka- 1209', 'NA', '1990-11-17', NULL, '2015-06-18 18:12:18', 'Team Member', 'NA', 'imran@rokomari.com', 'Md. Imranul Hasan', 'imran@rokomari.com.jpg', '2014-02-22', 'Imran', '2014-10-01', '01847103134', '1062', 'MALE', 'PERMANENT', '2015-06-24 16:35:42', 13, NULL),
(17, 'NA', 'House No- 7, Road- 10, Ward No- 9, Dogair Notun Para, Islambag, Demra, Dhaka- 1362.\r\n', 'NA', '1992-05-12', NULL, '2015-06-18 18:15:18', 'Team Member', 'NA', 'sazzad@rokomari.com', 'Md. Sazzad Hossain', 'sazzad@rokomari.com.jpg', '2014-05-01', 'Hridoy', NULL, '01675002789', '1078', 'MALE', 'TRIAL', '2015-06-24 16:05:09', 4, NULL),
(18, 'NA', '68/A, East Basabo, Dhaka.\r\n', 'NA', '1992-06-15', NULL, '2015-06-18 18:17:58', 'Team Member', 'NA', 'moon@rokomari.com', 'Mehedi Hasan Moon', 'moon@rokomari.com.jpg', '2015-02-01', 'Moon', NULL, '01675579145', '1124', 'MALE', 'INTERN', '2015-06-24 16:33:22', 7, NULL),
(19, 'NA', '142, 2nd Floor, South Komlapur, Dhaka.\r\n', 'NA', '1992-11-10', NULL, '2015-06-18 18:20:54', 'Team Member', 'B.Sc. In ICT', 'jakaria@rokomari.com', 'Md. Jakaria Islam', 'jakaria@rokomari.com.jpg', '2015-03-16', 'Jakaria', NULL, '01675747410', '1127', 'MALE', 'PROVISION', '2015-06-24 16:09:11', 9, NULL),
(20, 'NA', '142, 3rd Floor, South Komolapur, Dhaka.\r\n', 'NA', '1991-11-01', NULL, '2015-06-18 18:23:36', 'Team Member', 'B.Sc. In ICT', 'shougat@rokomari.com', 'Md. Shougat Hossain', 'shougat@rokomari.com.jpg', '2015-05-23', 'Shougat', NULL, '01781922299', '1132', 'MALE', 'PROVISION', '2015-06-24 16:09:49', 9, NULL),
(21, 'NA', 'House- 129, GP- Ga, Mohakhali, Dhaka.\r\n', 'NA', '1991-11-25', NULL, '2015-06-18 18:26:07', 'Team Member', 'B.Sc. In ICT', 'shakil@rokomari.com', 'Shakil Ahmed', 'shakil@rokomari.com.jpg', '2015-06-06', 'Shakil', NULL, '01722726121', '1134', 'MALE', 'PROVISION', '2015-06-24 16:10:00', 9, NULL),
(22, 'NA', 'Flat No: 303 (2nd Floor), 186 MK Cottage, 4/3/1, Hirajheel, RA, Shiddhirgonj, Narayanganj.\r\n', 'NA', '1992-01-01', NULL, '2015-06-18 18:28:22', 'Team Member', 'B.Sc. In ICT', 'johir@rokomari.com', 'Johirul Islam', 'johir@rokomari.com.jpg', '2015-06-06', 'Johir', NULL, '01834955744', '1135', 'MALE', 'PROVISION', '2015-06-24 16:08:40', 9, NULL),
(23, 'NA', 'Shefali Nibash, 85, Shamibag, Dhaka.\r\n', 'NA', '1990-09-12', NULL, '2015-06-20 13:49:30', 'Team Member', 'NA', 'shuvo@rokomari.com', 'Shuvo Kumar Das', 'shuvo@rokomari.com.jpg', '2012-02-10', 'Shuvo', '2012-08-01', '01833168195', '1016', 'MALE', 'PERMANENT', '2015-06-24 16:38:04', 10, NULL),
(24, 'i''m a creative artist of Bangladesh experienced in different art sector. Mentionable are Photography, Visual design, Film making, Motion graphic etc. i''m owner of design plan a design agency, Dot. Xtreme a film making agency, Blooming light a photo agency. i know excellent photo retouching and manipulation. i manipulated by the different Captured image using Adobe Photoshop i''m also interested on Information Technology and a successful freelancer works on design, software, web development etc. i inspire the young generation to be skillful on creative sector. For that cause i made video tutorial on Photoshop and Illustrator in Bangla sells sponsored by rokomari. i''m a creative entrepreneur also. Besides i''m working as a product photographer at Onnorokom group.', '255/A, Dolphin Tower (4D), West Shantibag, Dhaka.\r\n', 'NA', '1992-03-02', NULL, '2015-06-20 14:16:45', 'Team Member', 'NA', 'hemel@rokomari.com', 'Imran Hossain Hemel', 'hemel@rokomari.com.jpg', '2012-03-01', 'Hemel', '2012-07-01', '01684333979', '1017', 'MALE', 'PERMANENT', '2015-06-27 15:45:28', 1, ''),
(25, 'NA', '2/2E, Eden Complex, Arambag, Motijheel, Dhaka- 1000\r\n', 'NA', '1982-10-20', NULL, '2015-06-23 18:35:18', 'Team Member', 'NA', 'monirul@rokomari.com', 'Md. Monirul Islam', 'monirul@rokomari.com.jpg', '2011-10-24', 'Monir', '2012-03-01', '01734455014', '1003', 'MALE', 'PERMANENT', '2015-06-24 17:20:10', 5, NULL),
(26, 'NA', '2/2E, Eden Complex, Arambag, Motijheel, Dhaka- 1000\r\n', 'NA', '1990-07-15', NULL, '2015-06-23 18:40:11', 'Team Member', 'NA', 'palash@rokomari.com', 'Palash Roy', 'palash@rokomari.com.jpg', '2009-03-02', 'Monir', '2009-09-01', '01755131047', '1004', 'MALE', 'PERMANENT', '2015-06-24 17:21:47', 5, NULL),
(27, 'NA', '11/2- A, Kobi Jashim Uddin Road, Komola, Dhaka.\r\n\r\n', 'NA', '1988-08-25', NULL, '2015-06-23 18:41:52', 'Team Member', 'NA', 'mamun@rokomari.com', 'Md. Habibur Rahaman ', 'mamun@rokomari.com.jpg', '2008-11-25', 'Mamun', '2006-06-01', '01985721332', '1005', 'MALE', 'PERMANENT', '2015-06-25 11:33:04', 5, ''),
(28, 'NA', '2/2E, Eden Complex, Arambag, Motijheel, Dhaka- 1000\r\n\r\n', 'NA', '1994-06-08', NULL, '2015-06-23 18:44:47', 'Team Member', 'NA', 'zahid@rokomari.com', 'Zahid Hasan', 'zahid@rokomari.com.jpg', '2012-02-13', 'Zahid ', '2012-07-01', '01942565365', '1007', 'MALE', 'PERMANENT', '2015-06-24 17:22:02', 5, NULL),
(29, 'NA', 'B-76, G-11, A.G.B. Colony, Motijhil, Dhaka.\r\n\r\n\r\n', 'NA', '1990-05-15', NULL, '2015-06-23 18:47:50', 'Team Member', 'NA', 'mashudrana@rokomari.com', 'Md. Mashud Rana', 'mashudrana@rokomari.com.jpg', '2012-03-15', 'Mashud', '2012-07-01', '01763213781', '1006', 'MALE', 'PERMANENT', '2015-06-24 17:19:55', 5, NULL),
(30, 'NA', '2/2E, Eden Complex, Arambag, Motijheel, Dhaka- 1000\r\n', 'NA', '1994-01-03', NULL, '2015-06-24 10:16:54', 'Team Member', 'NA', 'abdullah@rokomari.com', 'Abdullah Khan', 'abdullah@rokomari.com.jpg', '2012-02-16', 'Abdullah', '2012-07-01', '01941743463', '1008', 'MALE', 'PERMANENT', '2015-06-25 18:15:52', 5, ''),
(31, 'NA', '2/2E, Eden Complex, Arambag, Motijheel, Dhaka- 1000\r\n\r\n', 'NA', '1984-06-06', NULL, '2015-06-24 10:18:45', 'Team Member', 'NA', 'kajol@rokomari.com', 'Kajol Roy', 'kajol@rokomari.com.jpg', '2012-07-17', 'Kajol', '2013-01-01', '01750456935', '1009', 'MALE', 'PERMANENT', '2015-06-24 17:18:21', 5, NULL),
(32, 'NA', '2/2E, Eden Complex, Arambag, Motijheel, Dhaka- 1000\r\n\r\n', 'NA', '1991-09-05', NULL, '2015-06-24 10:20:23', 'Team Member', 'NA', 'farulahmed@rokomari.com', 'Faruk Ahmed', 'farulahmed@rokomari.com.jpg', '2012-09-02', 'Faruk', '2011-03-01', '01746071946', '1010', 'MALE', 'PERMANENT', '2015-06-24 17:18:02', 5, NULL),
(33, 'NA', '10, South Komolapur, Motijhil, Dhaka.\r\n\r\n', 'NA', '1994-01-09', NULL, '2015-06-24 10:21:48', 'Team Member', 'NA', 'ripon@rokomari.com', 'Md. Ripon Sarkar', 'ripon@rokomari.com.jpg', '2012-11-01', 'Ripon', '2013-05-01', '01687812064', '1012', 'MALE', 'PERMANENT', '2015-06-24 17:20:32', 5, NULL),
(34, 'NA', '34, South Komolapur, Dhaka.\r\n\r\n\r\n', 'NA', '1981-10-04', NULL, '2015-06-24 10:23:17', 'Team Member', 'NA', 'towhid@rokomari.com', 'Mollah Towhidur Rahman', 'towhid@rokomari.com.jpg', '2013-02-22', 'Towhid', '2013-09-01', '01728199223', '1014', 'MALE', 'PERMANENT', '2015-06-24 17:21:27', 5, NULL),
(35, 'NA', '2/2E, Eden Complex, Arambag, Motijheel, Dhaka- 1000\r\n', 'NA', '1994-01-01', NULL, '2015-06-24 11:17:46', 'Team Member', 'NA', 'ataur@rokomari.com', 'Ataur Rahman', 'ataur@rokomari.com.jpg', '2013-03-15', 'Ataur', '2013-09-01', '01796836228', '1015', 'MALE', 'PERMANENT', '2015-06-24 17:17:43', 5, NULL),
(36, 'NA', '16/2, Bepasa Monzil, Golapbag, Dhaka- 1203.\r\n', 'NA', '1988-07-12', NULL, '2015-06-24 11:19:46', 'Team Member', 'NA', 'ealias@rokomari.com', 'Md. Ealias', 'ealias@rokomari.com.jpg', '2012-03-02', 'Ealias', '2012-07-01', '01923171116', '1019', 'MALE', 'PERMANENT', '2015-06-25 18:19:32', 5, ''),
(37, 'NA', 'House- 160/1, 5th Floor, Shantibag, Shahjahanpur, Dhaka- 1000.\r\n', 'NA', '1988-01-17', NULL, '2015-06-24 11:21:57', 'Team Member', 'NA', 'zillur@rokomari.com', 'Zillur Rahaman', 'zillur@rokomari.com.jpg', '2012-03-10', 'Zillur', '2012-07-01', '01729813826', '1020', 'MALE', 'PERMANENT', '2015-06-27 15:47:02', 4, ''),
(38, 'NA', 'B-76, G-11, A.G.B. Colony, Motijhil, Dhaka.\r\n', 'NA', '1991-03-02', NULL, '2015-06-24 11:26:45', 'Team Member', 'NA', 'rahat@rokomari.com', 'Md. Mahmud Alom Rahat', 'rahat@rokomari.com.jpg', '2013-02-07', 'Rahat', '2013-08-01', '01681820573', '1023', 'MALE', 'PERMANENT', '2015-06-24 16:41:12', 2, NULL),
(39, 'NA', 'B-76, G-11, A.G.B. Colony, Motijhil, Dhaka.\r\n', 'NA', '1985-10-26', NULL, '2015-06-24 11:28:33', 'Team Member', 'NA', 'jhumu@rokomari.com', 'Habiba Sultana Jhumu', 'jhumu@rokomari.com.jpg', '2013-03-02', 'Jhumu', '2013-09-01', '01732464872', '1024', 'FEMALE', 'LEFT', '2015-06-24 16:02:38', 4, NULL),
(40, 'NA', 'B-76, G-11, A.G.B. Colony, Motijhil, Dhaka- 1000.\r\n', 'NA', '1994-07-06', NULL, '2015-06-24 11:31:42', 'Team Member', 'NA', 'rockey@rokomari.com', 'M. Ashaduzzaman Rocky', 'rockey@rokomari.com.jpg', '2013-03-04', 'Rocky', NULL, '01742164232', '1025', 'MALE', 'INTERN', '2015-06-24 16:04:23', 4, NULL),
(41, 'NA', '41/2, Central Road, Dhanmondi, 1209.\r\n', 'NA', '1994-07-28', NULL, '2015-06-24 11:41:13', 'Team Member', 'NA', 'sajib@rokomari.com', 'Md.Sajib Ahmed', 'sajib@rokomari.com.jpg', '2012-06-01', 'Sajib', NULL, '01833168192', '1037', 'MALE', 'TRIAL', '2015-06-24 16:17:22', 7, NULL),
(42, 'NA', 'Dholaipar, Jatrabari, Dhaka.\r\n\r\n', 'NA', '1985-01-01', NULL, '2015-06-24 11:45:53', 'Team Member', 'NA', 'nazrul@rokomari.com', 'Md.Nazrul Islam', 'nazrul@rokomari.com.jpg', '2013-01-22', 'Nazrul', '2013-08-01', '01847103136', '1046', 'MALE', 'PERMANENT', '2015-06-24 16:29:07', 6, NULL),
(43, 'NA', '16, Tollabag, Sobhanbag, Dhaka- 1203.\r\n', 'NA', '1988-01-22', NULL, '2015-06-24 11:47:20', 'Team Member', 'NA', 'zafir@rokomari.com', 'Md. Javed Zafir', 'zafir@rokomari.com.jpg', '2014-11-13', 'Zafir', '2014-11-01', '01833168191', '1048', 'MALE', 'PERMANENT', '2015-06-27 15:55:13', 7, ''),
(44, 'NA', '113 (Ground Floor), Komolapur, Motijhil, Dhaka.\r\n', 'NA', '1997-11-20', NULL, '2015-06-24 12:03:51', 'Team Member', 'NA', 'shahidul@rokomari.com', 'Md. Shahidul Islam', 'shahidul@rokomari.com.jpg', '2014-03-01', 'Shahidul', '2014-09-01', '01768517300', '1058', 'MALE', 'TRIAL', '2015-06-24 15:16:24', 14, NULL),
(45, 'NA', 'Rayerbag, Shonir Akhra, Jatrabari, Dhaka.\r\n\r\n', 'NA', '1993-07-21', NULL, '2015-06-24 12:05:49', 'Team Member', 'NA', 'jahangir@rokomari.com', 'Md. Jahangir Alam', 'jahangir@rokomari.com.jpg', '2014-03-01', 'Jahangir', '2014-09-01', '01626050408', '1059', 'MALE', 'PERMANENT', '2015-06-24 17:19:32', 5, NULL),
(46, 'NA', '30 no. lakshmi Narayan Road, Deovog, Ward No.: 16, Narayanganj.\r\n\r\n', 'NA', '1986-06-05', NULL, '2015-06-24 12:07:41', 'Team Member', 'NA', 'arif@rokomari.com', 'Muhammad Ariful Islam', 'arif@rokomari.com.jpg', '2014-02-22', 'Ariful', '2015-01-01', '01847103132', '1063', 'MALE', 'PERMANENT', '2015-06-24 16:35:59', 13, NULL),
(47, 'NA', '16/A, East Tejturi, Bazar, Tejgaon, Dhaka- 1215.\r\n\r\n', 'NA', '1990-01-17', NULL, '2015-06-24 12:09:13', 'Team Member', 'NA', 'alamgir@rokomari.com', 'Md. Alamgir Hossen', 'alamgir@rokomari.com.jpg', '2014-05-15', 'Alamgir', '2015-01-01', '01912721804', '1064', 'MALE', 'PERMANENT', '2015-06-24 17:18:37', 5, NULL),
(48, 'NA', '12, South Komolapur, Dhaka.\r\n\r\n', 'NA', '1991-08-20', NULL, '2015-06-24 12:12:32', 'Team Member', 'NA', 'foysal@rokomari.com', 'Md. Iqbal Hossen Foysal', 'foysal@rokomari.com.jpg', '2014-05-19', 'Foysal', '2015-01-01', '01858386352', '1067', 'MALE', 'PERMANENT', '2015-06-24 17:19:14', 5, NULL),
(49, 'NA', 'NA', 'NA', '1985-10-25', NULL, '2015-06-24 12:15:31', 'Team Member', 'NA', 'saroar@rokomari.com', 'Saroar Hossain', 'saroar@rokomari.com.jpg', '2014-05-06', 'Saroar', '2014-12-01', '01754936324', '1073', 'MALE', 'PERMANENT', '2015-06-25 11:36:23', 14, ''),
(50, 'NA', 'NA', 'NA', '1985-10-25', NULL, '2015-06-24 12:18:33', 'Team Member', 'NA', 'masud@rokomari.com', 'Md. Mahmudul Haque', 'masud@rokomari.com.jpg', '2014-05-04', 'Mashud', '2015-01-01', '01917433271', '1074', 'MALE', 'PERMANENT', '2015-06-27 15:45:46', 2, ''),
(51, 'NA', '36/2, Quddus Vila, South Komolapur, Dhaka.\r\n', 'NA', '1988-08-01', NULL, '2015-06-24 12:20:49', 'Team Member', 'NA', 'billal@rokomari.com', 'Md. Billal Hosain', 'billal@rokomari.com.jpg', '2014-06-15', 'Billal', '2014-12-01', '01720694782', '1075', 'MALE', 'PERMANENT', '2015-06-24 16:45:46', 14, NULL),
(52, 'NA', 'House: 207/3, Bangladesh Air Force, Bongobondhu Ghati, Dhaka.\r\n\r\n', 'NA', '1992-10-24', NULL, '2015-06-24 12:22:21', 'Team Member', 'NA', ' hanif.rokomari@gmail.com', 'Md. Imam Hanif', 'imam@rokomari.com.jpg', '2014-07-01', 'Imam', '2015-01-01', '01919829076', '1076', 'MALE', 'PERMANENT', '2015-06-29 10:32:24', 1, ''),
(53, 'NA', '40/A, Road- 6, Sekhertek, Mohammadpur, Dhaka.\r\n', 'NA', '1992-09-10', NULL, '2015-06-24 12:24:11', 'Team Member', 'NA', 'urmee@rokomari.com', 'Jannatun Ferdousi Urmee', 'urmee@rokomari.com.jpg', '2014-03-13', 'Urmee', NULL, '01738 680196', '1077', 'FEMALE', 'TRIAL', '2015-06-24 16:03:00', 4, NULL),
(54, 'NA', 'B- 76, G- 11, AGB Colony, Motijheel, Dhaka.\r\n', 'NA', '1994-08-21', NULL, '2015-06-24 12:35:27', 'Team Member', 'NA', 'Joy@rokomari.com', 'Md. Mohiuddin Joy', 'Joy@rokomari.com.jpg', '2014-05-01', 'Joy', NULL, '01717380877', '1079', 'MALE', 'INTERN', '2015-06-24 16:04:57', 4, NULL),
(55, 'NA', 'Block- D, House-17, Road- 14, Mirpur- 11, Dhaka.\r\n', 'NA', '1990-01-01', NULL, '2015-06-24 12:42:27', 'Team Member', 'NA', 'imamhossen@rokomari.com', 'Imam Hossen', 'imamhossen@rokomari.com.jpg', '2014-06-01', 'Imam', '2014-12-01', '01989674686', '1084', 'MALE', 'PERMANENT', '2015-06-24 16:43:12', 14, NULL),
(56, 'NA', 'House- 48, Block- E, Road- 1, Kalshi, Mirpue- 11.5, Dhaka.\r\n', 'NA', '1994-06-22', NULL, '2015-06-24 12:44:22', 'Team Member', 'NA', 'Bijoy@rokomari.com', 'Md. Kamrul Islam', 'Bijoy@rokomari.com.jpg', '2014-06-04', 'Bijoy', '2014-12-01', '01751205654', '1083', 'MALE', 'PERMANENT', '2015-06-24 16:45:55', 14, NULL),
(57, 'NA', '113/10, West Sheorapara, Dhaka.\r\n', 'NA', '1995-12-31', NULL, '2015-06-24 12:47:52', 'Team Member', 'NA', 'Sohel@rokomari.com', 'Md. Sohel', 'Sohel@rokomari.com.jpg', '2014-05-24', 'Sohel', '2015-01-01', '01955696721', '1087', 'MALE', 'PERMANENT', '2015-06-24 17:20:54', 5, NULL),
(58, 'NA', '4th Floor, 4 no room, House- 150/2, South Komolapur, Dhaka.\r\n', 'NA', '1987-01-08', NULL, '2015-06-24 12:52:22', 'Team Member', 'NA', 'maruf@rokomari.com', 'Md. Maruf Hasan', 'maruf@rokomari.com.jpg', '2014-11-01', 'Maruf', '2015-02-01', '01818604992', '1089', 'MALE', 'PERMANENT', '2015-06-25 16:55:03', 11, ''),
(59, 'NA', '15/1, South Komolapur, Motijhil, Dhaka.\r\n', 'NA', '1991-07-01', NULL, '2015-06-24 12:54:09', 'Team Member', 'NA', 'alamin@rokomari.com', 'Md. Al Amin', 'alamin@rokomari.com.jpg', '2014-10-18', 'Ruman', NULL, '01712300047', '1090', 'MALE', 'PROVISION', '2015-06-24 16:04:34', 4, NULL),
(60, 'NA', '83/3, Uttor Mugda, Dhaka.\r\n\r\n', 'NA', '1990-12-15', NULL, '2015-06-24 12:56:13', 'Team Member', 'NA', 'nirob@rokomari.com', 'Nazmul Islam', 'nirob@rokomari.com.jpg', '2014-11-15', 'Nirob', NULL, '01722655521', '1091', 'MALE', 'PROVISION', '2015-06-25 11:34:46', 5, ''),
(61, 'NA', 'Uttor Pirerbag, 3/C, Sheorapara, Mirpur, Dhaka.\r\n', 'NA', '1991-04-18', NULL, '2015-06-24 12:57:29', 'Team Member', 'NA', 'aslamscm@gmail.com', 'Md. Aslam Firoz', 'aslamscm@gmail.com.jpg', '2014-10-16', 'Aslam', NULL, '01734579181', '1092', 'MALE', 'PROVISION', '2015-06-27 15:46:31', 5, ''),
(62, 'NA', '76, Uttor Jatrabari, Suti Khalpar Road, Jatrabari, Dhaka.\r\n\r\n', 'NA', '1996-10-25', NULL, '2015-06-24 12:59:04', 'Team Member', 'NA', 'zaman@gmail.com', 'Md. Badruzzaman', 'zaman@gmail.com.jpg', '2014-12-12', 'Zaman', NULL, '01703654478', '1094', 'MALE', 'TRIAL', '2015-06-24 15:09:54', 14, NULL),
(63, 'NA', '63, Suboldas Road, Chowdhury Bazar, Ajimpur, Dhaka.\r\n\r\n', 'NA', '1997-12-21', NULL, '2015-06-24 13:00:38', 'Team Member', 'NA', 'raita@rokomari.com', 'Raita Mehjabin', 'raita@rokomari.com.jpg', '2014-11-15', 'Raita', NULL, '01711664424', '1095', 'FEMALE', 'TRIAL', '2015-06-24 14:40:23', 4, NULL),
(64, 'NA', 'House- 43, Lane- 03, Merajnagar, D Block, Kadamtali, Dhaka- 1362.\r\n', 'NA', '1995-01-01', NULL, '2015-06-24 13:04:48', 'Team Member', 'NA', 'ihsan@rokomari.com', 'Ihsanul Hoque', 'ihsan@rokomari.com.jpg', '2014-05-01', 'Ihsan', '2014-12-01', '01912577988', '1096', 'MALE', 'PERMANENT', '2015-06-27 15:48:45', 13, ''),
(65, 'NA', 'Shahid Bongomata Sheikh Fazilatunnesa Muzib girls hostel, Eden women college, Azimpur, Dhaka- 1205.\r\n', 'NA', '1985-01-01', NULL, '2015-06-24 13:09:28', 'Team Member', 'NA', 'sorna@rokomari.com', 'Marufa Akter', 'sorna@rokomari.com.jpg', '2014-03-01', 'Sorna', NULL, '01929067370', '1097', 'FEMALE', 'INTERN', '2015-06-24 16:35:16', 13, NULL),
(66, 'NA', 'Shohid Nagar, 1 no. Goli, Lalbag, Dhaka.\r\n\r\n', 'NA', '1985-01-01', NULL, '2015-06-24 13:10:56', 'Team Member', 'NA', 'amir@rokomari.com', 'Amir Hossain', 'amir@rokomari.com.jpg', '2014-10-15', 'Amir', NULL, '01721333673', '1104', 'MALE', 'PROVISION', '2015-06-24 15:09:12', 14, NULL),
(67, 'NA', '3/13, Protap Das Lane, Singtola, Sutrapur, Dhaka- 1100.\r\n\r\n\r\n', 'NA', '1996-11-30', NULL, '2015-06-24 13:12:38', 'Team Member', 'NA', 'shohel@rokomari.com', 'Shah Shohel Ahmed', 'shohel@rokomari.com.jpg', '2014-12-18', 'Shohel', NULL, '01721333673', '1106', 'MALE', 'TRIAL', '2015-06-25 11:37:10', 5, ''),
(68, 'NA', '3/2, 3rd Floor, Nayapaltan, Dhaka.\r\n\r\n\r\n', 'NA', '1991-08-25', NULL, '2015-06-24 13:14:17', 'Team Member', 'NA', 'kashem@rokomari.com', 'Morsalin Bin Kashem', 'kashem@rokomari.com.jpg', '2015-01-01', 'Kashem', NULL, '01813661777', '1107', 'MALE', 'INTERN', '2015-06-25 11:34:04', 10, ''),
(69, 'NA', 'Na\r\n\r\n', 'NA', '1990-07-15', NULL, '2015-06-24 13:39:07', 'Team Member', 'NA', 'khadija@rokomari.com', 'Khadijatut Tahera', 'khadija@rokomari.com.jpg', '2015-01-01', 'Khadija', NULL, '01552396856', '1109', 'FEMALE', 'PROVISION', '2015-06-24 16:04:05', 4, NULL),
(70, 'NA', 'House-70, 2nd Floor, Doyagonj, Bottola, South Sayedabad, Dhaka.\r\n', 'NA', '1985-01-01', NULL, '2015-06-24 13:40:56', 'Team Member', 'NA', 'nahida@rokomari.com', 'Nahida Ahter', 'nahida@rokomari.com.jpg', '2014-12-01', 'Nahida', NULL, '01815222647', '1110', 'FEMALE', 'PROVISION', '2015-06-24 16:05:27', 4, NULL),
(71, 'NA', '28/1 (Mission Madani), Flat- 5/E, Mir Hazaribag, Gendaria, Dhaka.\r\n', 'NA', '1992-12-22', NULL, '2015-06-24 13:42:54', 'Team Member', 'NA', 'sumi@rokomari.com', 'Israt Zahan', 'sumi@rokomari.com.jpg', '2015-01-03', 'Sumi', NULL, '01682455222', '1111', 'FEMALE', 'PROVISION', '2015-06-24 16:02:50', 4, NULL),
(72, 'NA', '1/1, Agargao New Colony, Shere Bangla Nogor, Dhaka- 1207.\r\n', 'NA', '1994-11-25', NULL, '2015-06-24 13:44:24', 'Team Member', 'NA', 'arian@rokomari.com', 'Rajib Gandhi', 'arian@rokomari.com.jpg', '2014-12-29', 'Arian', NULL, '01685184317', '1112', 'MALE', 'TRIAL', '2015-06-24 15:09:31', 14, NULL),
(73, 'NA', 'Mirpur- 1, Block- C, Akbor Ali Masjider Pashe, Mirpur, Dhaka.\r\n', 'NA', '1987-10-24', NULL, '2015-06-24 13:46:05', 'Team Member', 'NA', 'zaman@rokomari.com', 'Md. Mahbub Zaman', 'zaman@rokomari.com.jpg', '2015-01-15', 'Zaman', NULL, '01729452079', '1113', 'MALE', 'TRIAL', '2015-06-24 15:14:29', 14, NULL),
(74, 'NA', '149/5, Arambag, Motijhil, Dhaka.\r\n', 'NA', '1992-04-05', NULL, '2015-06-24 13:48:29', 'Team Member', 'NA', 'millad@rokomari.com', 'Md. Millad Hossen', 'millad@rokomari.com.jpg', '2015-01-20', 'Millad', NULL, '01743117732', '1115', 'MALE', 'TRIAL', NULL, 14, NULL),
(75, 'NA', '113, South Komolapur, Dhaka.\r\n', 'NA', '1997-01-01', NULL, '2015-06-24 13:49:42', 'Team Member', 'NA', 'sumon@rokomari.com', 'Md. Yousuf Ali Sumon', 'sumon@rokomari.com.jpg', '2015-01-25', 'Sumon', NULL, '01718103122', '1116', 'MALE', 'TRIAL', NULL, 14, NULL),
(76, 'NA', 'Tony Tower, Jatrabari Chourasta, Jatrabari, Dhaka.\r\n', 'NA', '1994-12-27', NULL, '2015-06-24 13:51:12', 'Team Member', 'NA', 'mahfujur@rokomari.com', 'Md. Mahfujur Rahman', 'mahfujur@rokomari.com.jpg', '2015-02-01', 'iqbal', NULL, '01855527579', '1117', 'MALE', 'TRIAL', NULL, 7, NULL),
(77, 'NA', '40/A, D C Das Street, Lalbag, Dhaka, 1205\r\n', 'NA', '1995-10-07', NULL, '2015-06-24 13:53:42', 'Team Member', 'NA', 'iqbal@rokomari.com', 'S M Ishtiaq Chowdhury Echo', 'iqbal@rokomari.com.jpg', '2015-01-26', 'iqbal', NULL, '01671496051', '1118', 'MALE', 'TRIAL', '2015-06-27 15:48:22', 8, ''),
(78, 'NA', 'Sir A F Rahman Hall, Room No- 507, Dhaka University.\r\n', 'NA', '1994-12-30', NULL, '2015-06-24 13:55:41', 'Team Member', 'NA', 'delowar@rokomari.com', 'Delowar Ripon', 'delowar@rokomari.com.jpg', '2015-02-08', 'Ripon', NULL, '01760515951', '1119', 'MALE', 'INTERN', '2015-06-24 15:07:01', 10, NULL),
(80, 'NA', '40/D, South Komolapur, Dhaka.\r\n', 'NA', '1994-01-01', NULL, '2015-06-24 14:01:39', 'Team Member', 'NA', 'monir@rokomari.com', 'Md. Moniruzzaman', 'monir@rokomari.com.jpg', '2015-02-01', 'Monir', NULL, '01962779354', '1121', 'MALE', 'TRIAL', '2015-06-25 11:35:09', 14, ''),
(81, 'NA', 'F-14, Queens Garden Homes, 15 Iskaton Road, Dhaka.\r\n', 'NA', '1989-05-03', NULL, '2015-06-24 14:03:22', 'Team Member', 'NA', 'munir@rokomari.com', 'Sayed Munir Husain', 'munir@rokomari.com.jpg', '2015-03-02', 'Munir', NULL, '01710469465', '1122', 'MALE', 'LEFT', '2015-06-25 16:17:23', 5, ''),
(82, 'NA', '279/1, Batar Goli, Boro Mogbazar, Dhaka- 1217.\r\n', 'NA', '1993-07-28', NULL, '2015-06-24 14:05:03', 'Team Member', 'NA', 'mohammad@rokomari.com', 'Sheikh Mohammad Adnan', 'mohammad@rokomari.com.jpg', '2015-01-11', 'Adnan', NULL, '01680085534', '1123', 'MALE', 'TRIAL', '2015-06-25 11:32:17', 4, ''),
(83, 'NA', 'Masjid Goli, Near BNP Head Office, Naya Paltan, Dhaka.\r\n', 'NA', '1989-05-09', NULL, '2015-06-24 14:07:04', 'Team Member', 'NA', 'robin@rokomari.com', 'Mohammed Shamim Ahmed', 'robin@rokomari.com.jpg', '2015-03-01', 'Robin', NULL, '01536204230', '1125', 'MALE', 'PROVISION', '2015-06-25 11:36:09', 7, ''),
(84, 'NA', 'Na\r\n', 'NA', '1990-09-05', NULL, '2015-06-24 14:08:34', 'Team Member', 'NA', 'suchi@rokomari.com', 'Dewan Mahmuda Zaman', 'suchi@rokomari.com.jpg', '2015-03-15', 'Suchi', NULL, '01755790689', '1126', 'FEMALE', 'LEFT', NULL, 9, NULL),
(85, 'NA', 'Parliament Building, 1 no. Building, Manik Mia Avenue, Dhaka.\r\n', 'NA', '1993-03-20', NULL, '2015-06-24 14:12:19', 'Team Member', 'NA', 'zia@rokomari.com', 'Md. Ziaur Rahaman Zia', 'zia@rokomari.com.jpg', '2015-04-08', 'Zia', NULL, '01943061317', '1130', 'MALE', 'TRIAL', '2015-06-25 18:18:48', 5, ''),
(86, 'NA', '2/2E, Eden Complex, Arambag, Motijheel, Dhaka- 1000\r\n', 'NA', '1991-03-20', NULL, '2015-06-24 14:13:57', 'Team Member', 'NA', 'mofij@rokomari.com', 'Mofijul Islam', 'mofij@rokomari.com.jpg', '2015-05-25', 'Mofij', NULL, '01987367431', '1131', 'MALE', 'TRIAL', '2015-06-25 18:15:23', 5, ''),
(87, 'NA', 'Unique Job Aid, 4th Floor of Bushra Clinic, Mirpur- 10.\r\n', 'NA', '1988-03-18', NULL, '2015-06-24 14:16:16', 'Team Member', 'NA', 'khorshed@rokomari.com', 'Md. Khorshed Alam', 'khorshed@rokomari.com.jpg', '2015-01-06', 'Alam', NULL, '01794890033', '1133', 'MALE', 'TRIAL', '2015-06-25 18:17:14', 5, ''),
(88, 'NA', 'Arambag, Motijhil, Dhaka.\r\n\r\n', 'NA', '1990-11-11', NULL, '2015-06-24 14:18:58', 'Team Member', 'NA', 'alim@rokomari.com', 'Md. Abdul Alim', 'alim@rokomari.com.jpg', '2014-03-01', 'Alim', '2014-05-01', '01847103137', '1501', 'MALE', 'PERMANENT', '2015-06-24 16:43:32', 14, NULL),
(89, 'NA', '10, Shimulder Bari, Jagannath Shah Road, Lalbag, Dhaka.\r\n', 'Na', '1962-04-07', NULL, '2015-06-24 19:29:57', 'Team Member', 'NA', 'sahabuddin@rokomari.com', 'Md. Sahabuddin', 'sahabuddin@rokomari.com.jpg', '2014-02-01', 'Sahabuddin', '2014-08-01', '01766145788', '1051', 'MALE', 'PERMANENT', '2015-06-25 18:16:36', 15, 'Game'),
(90, 'NA', '2/2E, Eden Complex, Arambag, Motijheel, Dhaka- 1000\r\n', 'NA', '2001-11-19', NULL, '2015-06-24 19:38:19', 'Team Member', 'NA', 'rasel@rokomari.com', 'Md. Rasel', 'rasel@rokomari.com.jpg', '2014-10-16', 'Rasel', NULL, '01984768633', '1099', 'MALE', 'PROVISION', '2015-06-25 18:17:32', 15, 'Game'),
(91, 'NA', '2/2E, Eden Complex, Arambag, Motijheel, Dhaka- 1000\r\n', 'NA', '2007-05-08', NULL, '2015-06-24 19:42:48', 'Team Member', 'NA', 'riponmollah@rokomari.com', 'Md. Ripon Mollah', 'riponmollah@rokomari.com.jpg', '2015-03-29', 'Ripon', NULL, '01980702458', '1128', 'MALE', 'TRIAL', '2015-06-25 18:17:54', 15, 'Game'),
(95, 'NA', 'Nobodip Bosak Lane, Laxmi bazar, Dhaka.', 'NA', '1991-12-25', NULL, '2015-06-25 14:45:23', 'Team Member', 'NA', 'raju@rokomari.com', 'Abu Bokor Siddique Raju', 'raju@rokomari.com.jpg', '2013-02-01', 'Raju', '2014-02-05', '01847103131', '1042', 'MALE', 'PERMANENT', '2015-06-25 16:55:17', 8, 'Game'),
(96, 'NA', '2/2 E, Arambag Motijheel, Dhaka-1000 ', 'NA', '2001-01-01', NULL, '2015-06-25 19:29:48', 'Team Member', 'NA', 'rokib@rokomari.com', 'Md. Rokib Molla', 'rokib@rokomari.com.jpg', '2015-06-17', 'Rokib', NULL, '01984340459', '1136', 'MALE', 'TRIAL', '2015-06-25 19:30:10', 15, 'Game'),
(97, 'NA', 'MIRPUR -2 ,RAINKHOLA  BAZAR,  COMMERCE  COLLEGE  ROAD.\r\n', 'NA', '2000-01-01', NULL, '2015-06-29 11:04:54', 'Team Member', 'NA', 'hamid@rokomari.com', 'MD . SALMAN  HASAN', 'hamid@rokomari.com.jpg', '2014-01-01', 'HAMID', NULL, '01721430289', 'NA', 'MALE', 'PARTTIME', '2015-06-29 11:05:16', 16, 'Game'),
(98, 'NA', 'SEC -12 ,BLOCK –D , ROAD – 17 , HOUSE – 11 , PALLABI, DHAKA –1216 . ', 'NA', '2000-01-01', NULL, '2015-06-29 11:07:26', 'Team Member', 'NA', 'khokon@rokomari.com', 'MD.  BELASH  AHMED ', 'khokon@rokomari.com.jpg', '2014-01-01', 'KHOKON', NULL, '01738011049', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(99, 'NA', 'AZIZ VILLA  ,  88 , ROAD 6  MONSURABAD  DHAKA .', 'NA', '2000-01-01', NULL, '2015-06-29 11:09:37', 'Team Member', 'NA', 'laghan@rokomari.com', 'K .M  SAYEK  MAYSHAR  RABBI', 'laghan@rokomari.com.jpg', '2014-01-01', 'LAGHAN', NULL, '01711077101', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(100, 'NA', 'SARIF  MOHAMMAD  HALL  I. H. T   MOHAKHALI  DHAKA , 1213.\r\n', 'NA', '2000-01-01', NULL, '2015-06-29 11:11:50', 'Team Member', 'NA', 'mozammal@rokomari.com', 'MD.  IMRAN  MIAH', 'mozammal@rokomari.com.jpg', '2014-01-01', 'MOZAMMAL', NULL, '01934701175', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(101, 'NA', 'WEST  DHANMONDI  , ROAD NO – 15 , HOUSE NO- 111/1A', 'NA', '2000-01-01', NULL, '2015-06-29 11:13:52', 'Team Member', 'NA', 'monsur@rokomari.com', 'MD. ANAS KHAN ', 'monsur@rokomari.com.jpg', '2014-01-01', 'MONSUR', NULL, '01780588701 ', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(102, 'NA', 'WEST  DHANMONDI  , ROAD NO – 15 , HOUSE NO- 111/1A', 'NA', '2000-01-01', NULL, '2015-06-29 11:15:52', 'Team Member', 'NA', 'nayem@rokomari.com', 'S . M  ASMAUL  RAKIB ', 'nayem@rokomari.com.jpg', '2014-01-01', 'NAYEM', NULL, '01712486333', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(103, 'NA', 'MIRPUR   ORIGINAL  10 , BLOCK –A  , ROAD – 6 , WARD – 3 , HOUSE – 36 , 4TH FLOOR    \r\n', 'NA', '2000-01-01', NULL, '2015-06-29 11:17:27', 'Team Member', 'NA', 'pintu@rokomari.com', 'KAZI  MOJAHID  ALAM  DIPTO ', 'pintu@rokomari.com.jpg', '2014-01-01', 'PINTU', NULL, '01916110226', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(104, 'NA', 'MIIRPUR  1 \r\n', 'NA', '2000-01-01', NULL, '2015-06-29 11:19:09', 'Team Member', 'NA', 'rana.rasel@rokomari.com', 'RASEL  RANA', 'rana.rasel@rokomari.com.jpg', '2014-01-01', 'RANA', NULL, '01935521482', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(105, 'NA', '25/3   ZIGATOLA   DHANMONDI\r\n', 'NA', '2000-01-01', NULL, '2015-06-29 11:20:48', 'Team Member', 'NA', 'hasibur@rokomari.com', 'HASIBUL HASAN  TALUKDER', 'hasibur@rokomari.com.jpg', '2014-01-01', 'ROBIN', NULL, '01718084593', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(106, 'NA', 'HAZARI BAG B. D. R  . 5 NO . GATE', 'NA', '2000-01-01', NULL, '2015-06-29 11:22:40', 'Team Member', 'NA', 'shahin@rokomari.com', 'HASIBUL HASAN  TALUKDER', 'shahin@rokomari.com.jpg', '2014-01-01', 'SHAHIN', NULL, '01814974082 ', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(107, 'NA', 'MIRPUR – 2 , DHAKA 1216 ,  ZOO ROAD ,  BLOCK –A,  HOUSE NO- 91.', 'NA', '2000-01-01', NULL, '2015-06-29 11:24:27', 'Team Member', 'NA', 'salam@rokomari.com', 'MD . SALAM', 'salam@rokomari.com.jpg', '2014-01-01', 'SALAM', NULL, '01764081340', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(108, 'NA', 'MIRPUR – 2 , DHAKA 1216 ,  ZOO ROAD ,  BLOCK –A,  HOUSE NO- 91.', 'NA', '2000-01-01', NULL, '2015-06-29 11:26:09', 'Team Member', 'NA', 'selim@rokomari.com', 'MD . SAJJAD  HOSSAIN  BAPPY ', 'selim@rokomari.com.jpg', '2014-01-01', 'SELIM', NULL, '01734571113', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(110, 'NA', 'ROOM NO – 113 , SHARIF MUHAMMAD HALL , I. H . T  MOHAKHALI , DHAKA – 1213 ', 'NA', '2000-01-01', NULL, '2015-06-29 11:28:32', 'Team Member', 'NA', 'somik@rokomari.com', 'ABDULLAH  AL  MAMUN ', 'somik@rokomari.com.jpg', '2014-01-01', 'SOMIK', NULL, '01776465346', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(111, 'NA', 'INDERA ROAD   , DHAKA 1215 , HOUSE – 143 SAMIYA  MONZIL ', 'NA', '2000-01-01', NULL, '2015-06-29 11:30:43', 'Team Member', 'NA', 'talha@rokomari.com', 'M. A . ASHRAFI  BHUIYAN', 'talha@rokomari.com.jpg', '2014-01-01', 'TALHA', NULL, '01794667171', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(112, 'NA', 'SOUTH KUTUBKHALI , THANA – JATRABARI , DIST – DHAKA . \r\n', 'NA', '2000-01-01', NULL, '2015-06-29 11:32:03', 'Team Member', 'NA', 'toufiq@rokomari.com', 'MD . AMINUL ISLAM', 'toufiq@rokomari.com.jpg', '2014-01-01', 'TOUFIQ', NULL, '01521205367', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(113, 'NA', 'SOUTH KUTUBKHALI , THANA – JATRABARI , DIST – DHAKA . \r\n', 'NA', '2000-01-01', NULL, '2015-06-29 11:33:16', 'Team Member', 'NA', 'tayubur@rokomari.com', 'MD. TAYUBUR  RAHMAN', 'tayubur@rokomari.com.jpg', '2014-01-01', 'TAYUBUR', NULL, '01736102862', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game'),
(114, 'NA', 'VILL- JOYNAGOR , POST – ATI BAZAR , KERANIGONJ ,  DHAKA\r\n', 'NA', '2000-01-01', NULL, '2015-06-29 11:34:42', 'Team Member', 'NA', 'turab@rokomari.com', 'MD . HIJRAT HOSSAIN', 'turab@rokomari.com.jpg', '2014-01-01', 'TURAB', NULL, '01736102862', 'NA', 'MALE', 'PARTTIME', NULL, 16, 'Game');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` longtext NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `created`, `description`, `name`, `updated`) VALUES
(1, '2015-06-18 00:00:00', 'Admin Role', 'ADMIN', NULL),
(2, '2015-06-23 00:00:00', 'User Role', 'USER', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_info`
--

CREATE TABLE IF NOT EXISTS `site_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext,
  `content_bn` longtext,
  `created` datetime DEFAULT NULL,
  `title` longtext NOT NULL,
  `title_bn` longtext,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `site_info`
--

INSERT INTO `site_info` (`id`, `content`, `content_bn`, `created`, `title`, `title_bn`, `updated`) VALUES
(1, 'রকমারি ডট কমের শুরুটা হয়েছিল বাংলাদেশের সব মানুষকে বইয়ের আলোয় আলোকিত করার লক্ষ্য নিয়ে। আমাদের দেশের প্রকাশনা শিল্পটি যথেষ্ট সম্ভাবনাময় হওয়া সত্ত্বেও সঠিক বিপণন ব্যবস্থার অভাবে মুখ থুবড়ে পড়ে আছে। ঢাকার বাইরে বই তো রীতিমতো দুষ্প্রাপ্য বস্তু। আর ঢাকাতেও বই যেন কেবল শাহবাগ আর বাংলাবাজারের মধ্যে সীমাবদ্ধ হয়ে আছে। মানুষের কাছে বই পৌঁছে দিতে পারলে রাতারাতি পাল্টে যাবে বইয়ের বাজার চিত্র। এই ধারণা থেকেই রকমারির সূচনা বলে জানান তিনি। জাতি হিসেবে আমরা বই পড়তে খুব বেশি আগ্রহী নই। তাছাড়াও বিভিন্ন ধরণের বই এক জায়গায় পাওয়াও কষ্টকর একুশে বইমেলা ছাড়া। আর তাই রকমারি ডট কমের উৎপত্তি। কেননা এখানে আপনি বিজ্ঞান,সাহিত্য থেকে শুরু করে বিদেশী ভাষার অনুবাদ সবধরণের বই ই পাচ্ছেন।', NULL, NULL, 'Our Mission & Vision', 'আমাদের লক্ষ্য ও উদ্দেশ্য', '2015-06-22 18:37:59'),
(2, 'রকমারি ডট কম অন্যরকম ওয়েব সার্ভিস লিমিটেডের প্রধান অঙ্গসংগঠন। রকমারি ডট কম আনুষ্ঠানিকভাবে তার যাত্রা শুরু করে ১৯ শে জানুয়ারি, ২০১২ থেকে। এই তিন বছরে লক্ষাধিক বই পাঠকের কাছে পৌঁছে দিয়ে ও প্রায় এক লক্ষের বেশি বইয়ের সুবিশাল সংগ্রহ নিয়ে রকমারি ডট কম এগিয়ে যাচ্ছে দৃঢ়চিত্তে। প্রায় এক লক্ষ বইয়ের সুবিশাল সংগ্রহ আছে রকমারিতে। অনুবাদ,ইতিহাস ও ঐতিহ্য,ইংরেজি ভাষার বই,উপন্যাস,কবিতা,ফিকশন,নন-ফিকশন,খেলাধুলা এরকম প্রায় ৪০ টি ক্যাটাগরির লক্ষাধিক বই ক্রয় করা যাবে রকমারি থেকে। শুধু ক্যাটাগরিভিত্তিক শ্রেণীবিভাগই নয়, লেখক ভিত্তিক,প্রকাশনা সংস্থা ভিত্তিক,বইমেলা ভিত্তিক, প্রভৃতি অনেকগুলো শ্রেণিবিভাগ আপনার বই কেনাকে করে তুলবে সচ্ছন্দ ও আনন্দময়। ৩ বছরে প্রায় দেশ-বিদেশের লক্ষাধিক পাঠকের কাছে রকমারি ডট কম বই পৌঁছে দিয়েছে। ঢাকা থেকে পুরো বাংলাদেশে বইয়ের বিকেন্দ্রীকরণ রকমারির হাত ধরেই হয়েছে। এখন যে কেউ ঘরে বসেই যেকোনো বই হাতে পেয়ে যাচ্ছে রকমারি ডট কমের সাহায্যে।', NULL, NULL, 'The Story Behind Our Story', 'আমাদের গল্প', '2015-06-24 08:59:40'),
(3, 'রকমারি ডট কমে কর্মদক্ষতাকেও ছাপিয়ে যে জিনিসটি প্রতিটি টিম মেম্বারের মাঝে খোঁজ করা হয় সেটা হচ্ছে অফিস ও অফিসের অন্যান্য টিম মেম্বারদের সাথে মানিয়ে নেবার ক্ষমতা। রকমারি ডট কমে যেহেতু দলে কাজ করতে হয় তাই দলের প্রতিটি সদস্যকে দলের অন্যান্য সদস্যদের প্রতি সম্মান ও সম্প্রীতির দায় থাকতে হবে। দলের সিদ্ধান্তকে মেনে নিয়ে নিজের সেরাটা দেবার চেষ্টাও করতে হবে সবসময়। হাসি-ঠাট্টা,সুখ-আনন্দে কাজ শেষ হয়ে যাবে তাহলে নিজের অজান্তেই। প্রতিষ্ঠার শুরু থেকেই রকমারি ডট কম এখানে কর্মরত সকলের জন্য সাবলীল কর্মক্ষেত্র তৈরির চেষ্টা করে আসছে। অন্যরকম ওয়েব সার্ভিস লিমিটেডের প্রধান অঙ্গসংগঠন রকমারি কাজের ক্ষেত্রেও সেই ভিন্নতা ধরে রাখার চেষ্টা করেছে। আর এই ভিন্নতার ফলই তারা পাচ্ছে একটি সুখী প্রতিষ্ঠান হয়ে লক্ষাধিক মানুষের কাছে ‘পার্সেল অফ হ্যাপিনেস’ পৌঁছে দিয়ে।', 'রকমারি ডট কমে কর্মদক্ষতাকেও ছাপিয়ে যে জিনিসটি প্রতিটি টিম মেম্বারের মাঝে খোঁজ করা হয় সেটা হচ্ছে অফিস ও অফিসের অন্যান্য টিম মেম্বারদের সাথে মানিয়ে নেবার ক্ষমতা। রকমারি ডট কমে যেহেতু দলে কাজ করতে হয় তাই দলের প্রতিটি সদস্যকে দলের অন্যান্য সদস্যদের প্রতি সম্মান ও সম্প্রীতির দায় থাকতে হবে। দলের সিদ্ধান্তকে মেনে নিয়ে নিজের সেরাটা দেবার চেষ্টাও করতে হবে সবসময়। হাসি-ঠাট্টা,সুখ-আনন্দে কাজ শেষ হয়ে যাবে তাহলে নিজের অজান্তেই। প্রতিষ্ঠার শুরু থেকেই রকমারি ডট কম এখানে কর্মরত সকলের জন্য সাবলীল কর্মক্ষেত্র তৈরির চেষ্টা করে আসছে। অন্যরকম ওয়েব সার্ভিস লিমিটেডের প্রধান অঙ্গসংগঠন রকমারি কাজের ক্ষেত্রেও সেই ভিন্নতা ধরে রাখার চেষ্টা করেছে। আর এই ভিন্নতার ফলই তারা পাচ্ছে একটি সুখী প্রতিষ্ঠান হয়ে লক্ষাধিক মানুষের কাছে ‘পার্সেল অফ হ্যাপিনেস’ পৌঁছে দিয়ে।', '2015-06-13 00:00:00', 'Our Team & Culture', 'আমরা যেমন', NULL),
(4, '2/2 E,Arambag Motijheel, Dhaka-1000', NULL, NULL, 'Address', '??????', '2015-06-23 17:45:53'),
(5, 'info@rokomari.com', NULL, NULL, 'Email', '????', '2015-06-23 17:46:32'),
(6, '2/2 E, Arambag Motijheel, Dhaka-1000  Phone: 16297, 015 1952 1971  Email: admin@rokomari.com', '2/2 E,Arambag Motijheel, Dhaka-1000 Phone: 16297, 015 1952 1971 Email: admin@rokomari.com', '2015-06-13 00:00:00', 'Contact Us', '????????? ??????', NULL),
(7, '16297', NULL, NULL, 'Phone', '???', '2015-06-22 15:47:36'),
(8, 'রকমারি ডট কমের কর্মপন্থা সম্পূর্ণভাবেই দলভিত্তিক। রকমারির প্রতিটি কর্মচারিই একেকজন টিম মেম্বার। রকমারি বিশ্বাস করে যে দলের প্রত্যেকের অবদান ছাড়া কোন কাজ পরিপূর্ণভাবে করা সম্ভব নয়। রকমারিতে রয়েছে সাপ্লাই চেইন ও ডেলিভারি টিম,কাস্টমার কেয়ার টিম,ডেভেলপার টিম,বিজনেস ডেভেলপমেন্ট টিম,ডাটা ম্যানেজমেন্ট টিম,ইত্যাদি। এখানে কর্মরত সবাই কোন না কোন টিমের সদস্য। দলে কাজ করতে হয় বলেই কাজের দায়িত্বটুকু ভাগ হয়ে যায় সবার মাঝে যার ফলে কাজ আর কাজ থাকে না,হয়ে যায় আনন্দ মাধ্যম।', 'রকমারি ডট কমের কর্মপন্থা সম্পূর্ণভাবেই দলভিত্তিক। রকমারির প্রতিটি কর্মচারিই একেকজন টিম মেম্বার। রকমারি বিশ্বাস করে যে দলের প্রত্যেকের অবদান ছাড়া কোন কাজ পরিপূর্ণভাবে করা সম্ভব নয়। রকমারিতে রয়েছে সাপ্লাই চেইন ও ডেলিভারি টিম,কাস্টমার কেয়ার টিম,ডেভেলপার টিম,বিজনেস ডেভেলপমেন্ট টিম,ডাটা ম্যানেজমেন্ট টিম,ইত্যাদি। এখানে কর্মরত সবাই কোন না কোন টিমের সদস্য। দলে কাজ করতে হয় বলেই কাজের দায়িত্বটুকু ভাগ হয়ে যায় সবার মাঝে যার ফলে কাজ আর কাজ থাকে না,হয়ে যায় আনন্দ মাধ্যম। ', '2015-06-14 00:00:00', 'Meet Our Team', 'আমরা ক''জন', NULL),
(9, 'homeslider1.jpg', NULL, NULL, 'homeslider1', '', '2015-06-24 09:55:10'),
(10, 'homeslider2.jpg', NULL, NULL, 'homeslider2', '', '2015-06-23 15:58:29'),
(11, 'homeslider3.jpg', NULL, NULL, 'homeslider3', '', '2015-06-23 15:58:37'),
(12, 'homeslider4.jpg', NULL, NULL, 'homeslider4', '', '2015-06-23 15:58:46'),
(13, 'homeslider5.jpg', NULL, NULL, 'homeslider5', '', '2015-06-23 16:01:11'),
(14, 'ourdeptimg1.jpg', NULL, NULL, 'deptslider1', NULL, NULL),
(15, 'ourdeptimg2.jpg', NULL, NULL, 'deptslider2', NULL, NULL),
(16, 'ourdeptimg3.jpg', NULL, NULL, 'deptslider3', NULL, NULL),
(17, 'ourdeptimg4.jpg', NULL, NULL, 'deptslider4', NULL, NULL),
(18, 'ourdeptimg5.jpg', NULL, NULL, 'deptslider5', NULL, NULL),
(19, 'teamslider1.jpg', NULL, NULL, 'teamslider1', '', '2015-06-23 16:03:44'),
(20, 'teamslider2.jpg', NULL, NULL, 'teamslider2', '', '2015-06-23 16:03:54'),
(21, 'teamslider3.jpg', NULL, NULL, 'teamslider3', '', '2015-06-23 16:04:04'),
(22, 'teamslider4.jpg', NULL, NULL, 'teamslider4', '', '2015-06-23 16:04:12'),
(23, 'teamslider5.jpg', NULL, NULL, 'teamslider5', '', '2015-06-23 16:04:55'),
(24, 'রকমারি ডট কম ৩ বছরের পথ পরিক্রমায় লক্ষাধিক পাঠক/ক্রেতার কাছে “পার্সেল অফ হ্যাপিনেস” পৌঁছে দিয়েছে। আর এই লক্ষাধিক পার্সেলের পেছনে লুকিয়ে আছে রকমারির প্রতিটি বিভাগ,টিম ও টিম মেম্বারদের অজানা অনেক গল্প। ক্রেতাদের সর্বোচ্চ সুবিধা নিশ্চিত করার জন্য রকমারি ডট কমকে অনেকগুলো বিভাগে বিভক্ত হয়ে কাজ করতে হয়েছে। এই বিভাগগুলোর মাঝে আছে একাউন্ট অ্যান্ড ফিন্যান্স,বিজনেস ডেভেলপমেন্ট,কাস্টমার কেয়ার,ডাটা ম্যানেজমেন্ট,গ্রাফিক্স অ্যান্ড ক্রিয়েটিভ,হিউম্যান রিসোর্স,আইটি অ্যান্ড সাপোর্ট,মার্কেটিং অ্যান্ড ব্র্যান্ডিং,মার্চেন্ট অ্যান্ড প্রোডাক্ট ম্যানেজমেন্ট,পাবলিক রিলেশন,রকমারি কুরিয়ার,সফটওয়্যার ইঞ্জিনিয়ারিং,সাপ্লাই চেইন অ্যান্ড ম্যানেজমেন্ট,ইত্যাদি। চলুন জেনে নেয়া যাক এই বিভাগগুলোর কাজের ক্ষেত্র ও এদের অজানা কিছু গল্প...  ', 'রকমারি ডট কম ৩ বছরের পথ পরিক্রমায় লক্ষাধিক পাঠক/ক্রেতার কাছে “পার্সেল অফ হ্যাপিনেস” পৌঁছে দিয়েছে। আর এই লক্ষাধিক পার্সেলের পেছনে লুকিয়ে আছে রকমারির প্রতিটি বিভাগ,টিম ও টিম মেম্বারদের অজানা অনেক গল্প। ক্রেতাদের সর্বোচ্চ সুবিধা নিশ্চিত করার জন্য রকমারি ডট কমকে অনেকগুলো বিভাগে বিভক্ত হয়ে কাজ করতে হয়েছে। এই বিভাগগুলোর মাঝে আছে একাউন্ট অ্যান্ড ফিন্যান্স,বিজনেস ডেভেলপমেন্ট,কাস্টমার কেয়ার,ডাটা ম্যানেজমেন্ট,গ্রাফিক্স অ্যান্ড ক্রিয়েটিভ,হিউম্যান রিসোর্স,আইটি অ্যান্ড সাপোর্ট,মার্কেটিং অ্যান্ড ব্র্যান্ডিং,মার্চেন্ট অ্যান্ড প্রোডাক্ট ম্যানেজমেন্ট,পাবলিক রিলেশন,রকমারি কুরিয়ার,সফটওয়্যার ইঞ্জিনিয়ারিং,সাপ্লাই চেইন অ্যান্ড ম্যানেজমেন্ট,ইত্যাদি। চলুন জেনে নেয়া যাক এই বিভাগগুলোর কাজের ক্ষেত্র ও এদের অজানা কিছু গল্প...  ', NULL, 'আমাদের বিভাগ সমূহ', 'আমাদের বিভাগ সমূহ', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `created`, `email`, `password`, `status`, `updated`) VALUES
(2, '2015-06-24 16:09:34', 'jahid@rokomari.com', 'Jahid', 'PENDING', '2015-06-24 16:09:34'),
(3, '2015-06-24 16:13:11', 'kabir@rokomari.com', 'Kabir', 'PENDING', '2015-06-24 16:13:11'),
(4, '2015-06-29 10:31:50', 'malak@rokomari.com', 'Malak', 'PENDING', '2015-06-29 10:31:50'),
(5, '2015-06-24 17:20:40', 'tipu@rokomari.com', 'Tipu', 'PENDING', '2015-06-24 17:20:40'),
(6, '2015-06-29 11:36:10', 'masum@rokomari.com', 'Masum', 'PENDING', '2015-06-29 11:36:10'),
(7, '2015-06-24 15:52:30', 'bonie@rokomari.com', '12345', 'PENDING', '2015-06-24 15:52:30'),
(8, '2015-06-24 16:40:33', 'jimy@rokomari.com', 'Jimy', 'PENDING', '2015-06-24 16:40:33'),
(9, '2015-06-24 16:40:24', 'sweet@rokomari.com', 'Sweet', 'PENDING', '2015-06-24 16:40:24'),
(10, '2015-06-24 16:40:44', 'faruk@rokomari.com', 'Faruk', 'PENDING', '2015-06-24 16:40:44'),
(11, '2015-06-24 16:29:25', 'jahurul@rokomari.com', 'Miraj', 'PENDING', '2015-06-24 16:29:25'),
(12, '2015-06-24 16:33:34', 'rana@rokomari.com', 'Rana', 'PENDING', '2015-06-24 16:33:34'),
(13, '2015-06-24 16:35:30', 'adnan@rokomari.com', 'Adnan', 'PENDING', '2015-06-24 16:35:30'),
(14, '2015-06-24 16:14:05', 'sadi@rokomari.com', 'Sadi', 'PENDING', '2015-06-24 16:14:05'),
(15, '2015-06-24 16:43:01', 'bahar@rokomari.com', 'Bahar', 'PENDING', '2015-06-24 16:43:01'),
(16, '2015-06-24 16:35:42', 'imran@rokomari.com', 'Imran', 'PENDING', '2015-06-24 16:35:42'),
(17, '2015-06-24 16:05:09', 'sazzad@rokomari.com', 'Hridoy', 'PENDING', '2015-06-24 16:05:09'),
(18, '2015-06-24 16:33:22', 'moon@rokomari.com', 'Moon', 'PENDING', '2015-06-24 16:33:22'),
(19, '2015-06-25 11:55:01', 'jakaria@rokomari.com', '12345', 'ACTIVE', '2015-06-25 11:55:01'),
(20, '2015-06-24 16:09:49', 'shougat@rokomari.com', 'Shougat', 'PENDING', '2015-06-24 16:09:49'),
(21, '2015-06-24 16:10:00', 'shakil@rokomari.com', 'Shakil', 'PENDING', '2015-06-24 16:10:00'),
(22, '2015-06-24 16:08:40', 'johir@rokomari.com', 'Johir', 'PENDING', '2015-06-24 16:08:40'),
(23, '2015-06-24 16:38:04', 'shuvo@rokomari.com', 'Shuvo', 'PENDING', '2015-06-24 16:38:04'),
(24, '2015-06-27 15:45:28', 'hemel@rokomari.com', 'Hemel', 'PENDING', '2015-06-27 15:45:28'),
(25, '2015-06-24 17:20:10', 'monirul@rokomari.com', 'Monir', 'PENDING', '2015-06-24 17:20:10'),
(26, '2015-06-24 17:21:47', 'palash@rokomari.com', 'Monir', 'PENDING', '2015-06-24 17:21:47'),
(27, '2015-06-25 11:33:04', 'mamun@rokomari.com', 'Mamun', 'PENDING', '2015-06-25 11:33:04'),
(28, '2015-06-24 17:22:02', 'zahid@rokomari.com', 'Zahid ', 'PENDING', '2015-06-24 17:22:02'),
(29, '2015-06-24 17:19:55', 'mashudrana@rokomari.com', 'Mashud', 'PENDING', '2015-06-24 17:19:55'),
(30, '2015-06-25 18:15:52', 'abdullah@rokomari.com', 'Abdullah', 'PENDING', '2015-06-25 18:15:52'),
(31, '2015-06-24 17:18:21', 'kajol@rokomari.com', 'Kajol', 'PENDING', '2015-06-24 17:18:21'),
(32, '2015-06-24 17:18:02', 'farulahmed@rokomari.com', 'Faruk', 'PENDING', '2015-06-24 17:18:02'),
(33, '2015-06-24 17:20:32', 'ripon@rokomari.com', 'Ripon', 'PENDING', '2015-06-24 17:20:32'),
(34, '2015-06-24 17:21:27', 'towhid@rokomari.com', 'Towhid', 'PENDING', '2015-06-24 17:21:27'),
(35, '2015-06-24 17:17:43', 'ataur@rokomari.com', 'Ataur', 'PENDING', '2015-06-24 17:17:43'),
(36, '2015-06-25 18:19:32', 'ealias@rokomari.com', 'Ealias', 'PENDING', '2015-06-25 18:19:32'),
(37, '2015-06-27 15:47:02', 'zillur@rokomari.com', 'Zillur', 'PENDING', '2015-06-27 15:47:03'),
(38, '2015-06-24 16:41:12', 'rahat@rokomari.com', 'Rahat', 'PENDING', '2015-06-24 16:41:12'),
(39, '2015-06-24 16:02:38', 'jhumu@rokomari.com', 'Jhumu', 'PENDING', '2015-06-24 16:02:38'),
(40, '2015-06-24 16:04:23', 'rockey@rokomari.com', 'Rocky', 'PENDING', '2015-06-24 16:04:23'),
(41, '2015-06-24 16:17:22', 'sajib@rokomari.com', 'Sajib', 'PENDING', '2015-06-24 16:17:22'),
(42, '2015-06-24 16:29:07', 'nazrul@rokomari.com', 'Nazrul', 'PENDING', '2015-06-24 16:29:07'),
(43, '2015-06-27 15:55:13', 'zafir@rokomari.com', 'Zafir', 'PENDING', '2015-06-27 15:55:14'),
(44, '2015-06-24 15:49:39', 'shahidul@rokomari.com', '12345', 'PENDING', '2015-06-24 15:49:39'),
(45, '2015-06-24 17:19:32', 'jahangir@rokomari.com', 'Jahangir', 'PENDING', '2015-06-24 17:19:32'),
(46, '2015-06-24 16:35:59', 'arif@rokomari.com', 'Ariful', 'PENDING', '2015-06-24 16:35:59'),
(47, '2015-06-24 17:18:37', 'alamgir@rokomari.com', 'Alamgir', 'PENDING', '2015-06-24 17:18:38'),
(48, '2015-06-24 17:19:14', 'foysal@rokomari.com', 'Foysal', 'PENDING', '2015-06-24 17:19:14'),
(49, '2015-06-25 11:36:23', 'saroar@rokomari.com', 'Saroar', 'PENDING', '2015-06-25 11:36:23'),
(50, '2015-06-27 15:45:46', 'masud@rokomari.com', 'Mashud', 'PENDING', '2015-06-27 15:45:46'),
(51, '2015-06-24 16:45:46', 'billal@rokomari.com', 'Billal', 'PENDING', '2015-06-24 16:45:46'),
(52, '2015-06-29 10:32:24', ' hanif.rokomari@gmail.com', 'Imam', 'PENDING', '2015-06-29 10:32:24'),
(53, '2015-06-24 16:03:00', 'urmee@rokomari.com', 'Urmee', 'PENDING', '2015-06-24 16:03:00'),
(54, '2015-06-24 16:04:57', 'Joy@rokomari.com', 'Joy', 'PENDING', '2015-06-24 16:04:57'),
(55, '2015-06-24 16:43:12', 'imamhossen@rokomari.com', 'Imam', 'PENDING', '2015-06-24 16:43:12'),
(56, '2015-06-24 16:45:55', 'Bijoy@rokomari.com', 'Bijoy', 'PENDING', '2015-06-24 16:45:55'),
(57, '2015-06-24 17:20:54', 'Sohel@rokomari.com', 'Sohel', 'PENDING', '2015-06-24 17:20:54'),
(58, '2015-06-25 16:55:03', 'maruf@rokomari.com', 'Maruf', 'PENDING', '2015-06-25 16:55:03'),
(59, '2015-06-24 16:04:34', 'alamin@rokomari.com', 'Ruman', 'PENDING', '2015-06-24 16:04:34'),
(60, '2015-06-25 11:34:46', 'nirob@rokomari.com', 'Nirob', 'PENDING', '2015-06-25 11:34:46'),
(61, '2015-06-27 15:46:31', 'aslamscm@gmail.com', 'Aslam', 'PENDING', '2015-06-27 15:46:31'),
(62, '2015-06-24 15:41:02', 'zaman@gmail.com', '12345', 'PENDING', '2015-06-24 15:41:02'),
(63, '2015-06-24 15:51:59', 'raita@rokomari.com', '12345', 'PENDING', '2015-06-24 15:51:59'),
(64, '2015-06-27 15:48:45', 'ihsan@rokomari.com', 'Ihsan', 'PENDING', '2015-06-27 15:48:45'),
(65, '2015-06-24 16:35:16', 'sorna@rokomari.com', 'Sorna', 'PENDING', '2015-06-24 16:35:16'),
(66, '2015-06-24 15:36:12', 'amir@rokomari.com', '12345', 'PENDING', '2015-06-24 15:36:12'),
(67, '2015-06-25 11:37:10', 'shohel@rokomari.com', 'Shohel', 'PENDING', '2015-06-25 11:37:10'),
(68, '2015-06-25 11:34:04', 'kashem@rokomari.com', 'Kashem', 'PENDING', '2015-06-25 11:34:04'),
(69, '2015-06-24 16:04:05', 'khadija@rokomari.com', 'Khadija', 'PENDING', '2015-06-24 16:04:05'),
(70, '2015-06-24 16:05:27', 'nahida@rokomari.com', 'Nahida', 'PENDING', '2015-06-24 16:05:27'),
(71, '2015-06-24 16:02:50', 'sumi@rokomari.com', 'Sumi', 'PENDING', '2015-06-24 16:02:50'),
(72, '2015-06-24 15:52:02', 'arian@rokomari.com', '12345', 'PENDING', '2015-06-24 15:52:02'),
(73, '2015-06-24 15:45:17', 'zaman@rokomari.com', '12345', 'PENDING', '2015-06-24 15:45:17'),
(74, '2015-06-24 15:47:27', 'millad@rokomari.com', '12345', 'PENDING', '2015-06-24 15:47:27'),
(75, '2015-06-24 15:50:39', 'sumon@rokomari.com', '12345', 'PENDING', '2015-06-24 15:50:39'),
(76, '2015-06-24 16:15:03', 'mahfujur@rokomari.com', 'iqbal', 'PENDING', '2015-06-24 16:15:03'),
(77, '2015-06-27 15:48:22', 'iqbal@rokomari.com', 'iqbal', 'PENDING', '2015-06-27 15:48:22'),
(78, '2015-06-24 15:36:40', 'delowar@rokomari.com', '12345', 'PENDING', '2015-06-24 15:36:40'),
(79, '2015-06-24 16:28:37', 'kinkor@rokomari.com', 'Kinkor', 'PENDING', '2015-06-24 16:28:37'),
(80, '2015-06-25 11:35:09', 'monir@rokomari.com', 'Monir', 'PENDING', '2015-06-25 11:35:09'),
(81, '2015-06-25 16:17:23', 'munir@rokomari.com', 'Munir', 'PENDING', '2015-06-25 16:17:23'),
(82, '2015-06-25 11:32:17', 'mohammad@rokomari.com', 'Adnan', 'PENDING', '2015-06-25 11:32:17'),
(83, '2015-06-25 11:36:09', 'robin@rokomari.com', 'Robin', 'PENDING', '2015-06-25 11:36:09'),
(84, '2015-06-24 15:36:56', 'suchi@rokomari.com', '12345', 'PENDING', '2015-06-24 15:36:56'),
(85, '2015-06-25 18:18:48', 'zia@rokomari.com', 'Zia', 'PENDING', '2015-06-25 18:18:49'),
(86, '2015-06-25 18:15:23', 'mofij@rokomari.com', 'Mofij', 'PENDING', '2015-06-25 18:15:24'),
(87, '2015-06-25 18:17:14', 'khorshed@rokomari.com', 'Alam', 'PENDING', '2015-06-25 18:17:14'),
(88, '2015-06-24 16:43:32', 'alim@rokomari.com', 'Alim', 'PENDING', '2015-06-24 16:43:32'),
(89, '2015-06-25 18:16:36', 'sahabuddin@rokomari.com', 'Sahabuddin', 'PENDING', '2015-06-25 18:16:36'),
(90, '2015-06-25 18:17:32', 'rasel@rokomari.com', 'Rasel', 'PENDING', '2015-06-25 18:17:32'),
(91, '2015-06-25 18:17:54', 'riponmollah@rokomari.com', 'Ripon', 'PENDING', '2015-06-25 18:17:54'),
(95, '2015-06-25 16:55:17', 'raju@rokomari.com', 'Raju', 'PENDING', '2015-06-25 16:55:17'),
(96, '2015-06-25 19:30:10', 'rokib@rokomari.com', 'Rokib', 'PENDING', '2015-06-25 19:30:10'),
(97, '2015-06-29 11:05:16', 'hamid@rokomari.com', 'HAMID', 'PENDING', '2015-06-29 11:05:16'),
(98, '2015-06-29 11:07:26', 'khokon@rokomari.com', 'KHOKON', 'PENDING', NULL),
(99, '2015-06-29 11:09:37', 'laghan@rokomari.com', 'LAGHAN', 'PENDING', NULL),
(100, '2015-06-29 11:11:50', 'mozammal@rokomari.com', 'MOZAMMAL', 'PENDING', NULL),
(101, '2015-06-29 11:13:53', 'monsur@rokomari.com', 'MONSUR', 'PENDING', NULL),
(102, '2015-06-29 11:15:53', 'nayem@rokomari.com', 'nayem', 'PENDING', NULL),
(103, '2015-06-29 11:17:27', 'pintu@rokomari.com', 'PINTU', 'PENDING', NULL),
(104, '2015-06-29 11:19:09', 'rana.rasel@rokomari.com', 'RANA', 'PENDING', NULL),
(105, '2015-06-29 11:20:48', 'hasibur@rokomari.com', 'ROBIN', 'PENDING', NULL),
(106, '2015-06-29 11:22:41', 'shahin@rokomari.com', 'SHAHIN', 'PENDING', NULL),
(107, '2015-06-29 11:24:27', 'salam@rokomari.com', 'SALAM', 'PENDING', NULL),
(108, '2015-06-29 11:26:09', 'selim@rokomari.com', 'SELIM', 'PENDING', NULL),
(110, '2015-06-29 11:28:33', 'somik@rokomari.com', 'SOMIK', 'PENDING', NULL),
(111, '2015-06-29 11:30:43', 'talha@rokomari.com', 'TALHA', 'PENDING', NULL),
(112, '2015-06-29 11:32:03', 'toufiq@rokomari.com', 'TOUFIQ', 'PENDING', NULL),
(113, '2015-06-29 11:33:16', 'tayubur@rokomari.com', 'TAYUBUR', 'PENDING', NULL),
(114, '2015-06-29 11:34:42', 'turab@rokomari.com', 'TURAB', 'PENDING', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE IF NOT EXISTS `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  KEY `FK_it77eq964jhfqtu54081ebtio` (`role_id`),
  KEY `FK_apcc8lxk2xnug8377fatvbn04` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
(2, 2),
(3, 2),
(5, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 1),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(25, 2),
(26, 2),
(27, 2),
(28, 2),
(29, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(35, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(44, 2),
(45, 2),
(46, 2),
(47, 2),
(48, 2),
(49, 2),
(51, 2),
(53, 2),
(54, 2),
(55, 2),
(56, 2),
(57, 2),
(59, 2),
(60, 2),
(62, 2),
(63, 2),
(65, 2),
(66, 2),
(67, 2),
(68, 2),
(69, 2),
(70, 2),
(71, 2),
(72, 2),
(73, 2),
(74, 2),
(75, 2),
(76, 2),
(78, 2),
(79, 2),
(80, 2),
(82, 2),
(83, 2),
(84, 2),
(88, 2),
(81, 2),
(58, 2),
(95, 2),
(86, 2),
(30, 2),
(89, 2),
(87, 2),
(90, 2),
(91, 2),
(85, 2),
(36, 2),
(96, 2),
(24, 2),
(50, 2),
(61, 2),
(37, 2),
(77, 2),
(64, 2),
(43, 2),
(4, 2),
(52, 2),
(97, 2),
(98, 2),
(99, 2),
(100, 2),
(101, 2),
(102, 2),
(103, 2),
(104, 2),
(105, 2),
(106, 2),
(107, 2),
(108, 2),
(110, 2),
(111, 2),
(112, 2),
(113, 2),
(114, 2),
(6, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `FK_ak32q581e33b0ouqitj0i07b1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`);

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `FK_apcc8lxk2xnug8377fatvbn04` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_it77eq964jhfqtu54081ebtio` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
