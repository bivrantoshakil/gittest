$(document).ready(function(){    
    $(window).bind('scroll', function() {
        if ($(window).scrollTop() > 50) {
            $('header').css({'height' : '50px','transition' : 'height 1s'});
			jQuery("ul#menu li").each(function() {
			   var $li = jQuery(this);
			   $li.css({'margin-top' : '11px','transition' : 'margin-top 1s'});       
			});
			//console.log($(window).scrollTop());
        }
        else {
            $('header').css({'height' : '100px','transition' : 'height 1s'});
			jQuery("ul#menu li").each(function() {
			   var $li = jQuery(this);
			   $li.css({'margin-top' : '36px','transition' : 'margin-top 1s'});       
			});
        }
    });
});